#include <iostream>
#include <vector>
#include <GL/glut.h>
#include <cmath>
#include "IRG/Matrix.h"

void reshape(int width, int height);
void display();
void renderScene();

void keyPressed(unsigned char key, int x, int y);
void mousePressedOrReleased(int button, int state, int x, int y);
void mouseMoved(int x, int y);

void renderPolygon();
int findClosestPoint(int x, int y);

struct Point {
    int x, y;
};

void computeFactors(std::vector<int>& factors);
void drawBezier(std::vector<Point>& points);
void computeInterpolated();

std::vector<Point> points;
std::vector<Point> interpolated;

const int NO_DRAG = -1;
int dragging_index = NO_DRAG;

#define DIVS_DEFAULT 50
#define DIVS_MIN 1
#define DIVS_MAX 100
int divs = DIVS_DEFAULT;

bool mode[] = {true, true, true};

int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE);
    glutInitWindowSize(800, 600);
    glutInitWindowPosition(400, 100);
    glutCreateWindow("Task");

    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutKeyboardFunc(keyPressed);
    glutMouseFunc(mousePressedOrReleased);
    glutPassiveMotionFunc(mouseMoved);

    glutMainLoop();
    return 0;
}

void reshape(int width, int height)
{
    glDisable(GL_DEPTH_TEST);
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, width-1, height-1, 0, 0, 1);
    glMatrixMode(GL_MODELVIEW);
}

void display()
{
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    glLoadIdentity();
    renderScene();
    glutSwapBuffers();
}

void renderScene()
{
    if (mode[0]) {
        glColor3f(1.0f, 0.0f, 0.0f);
        renderPolygon();
    }
    if (mode[1]) {
        glColor3f(0.0f, 0.0f, 1.0f);
        drawBezier(points);
    }
    if (mode[2]) {
        glColor3f(0.0f, 0.0f, 0.0f);
        computeInterpolated();
        drawBezier(interpolated);
    }
}

void keyPressed(unsigned char key, int x, int y)
{
    if (key == 27) {
        points.clear();
        dragging_index = NO_DRAG;
        glutPostRedisplay();
    } else if (key == 'm') {
        if (dragging_index == NO_DRAG) {
            dragging_index = findClosestPoint(x, y);
            if (dragging_index == NO_DRAG) {
                std::cout << "No close points detected." << std::endl;
            } else {
                points[dragging_index] = {x, y};
                glutPostRedisplay();
            }
        } else {
            std::cout << "Already dragging point at index "
                << dragging_index << '.' << std::endl;
        }
    } else if (key == '+') {
        divs ++;
        if (divs > DIVS_MAX) divs = DIVS_MAX;
        else glutPostRedisplay();
    } else if (key == '-') {
        divs --;
        if (divs < DIVS_MIN) divs = DIVS_MIN;
        else glutPostRedisplay();
    } else if (key >= '1' && key <= '3') {
        mode[key-'1'] = !mode[key-'1'];
        glutPostRedisplay();
    }
}

void mousePressedOrReleased(int button, int state, int x, int y)
{
    if (button == GLUT_LEFT_BUTTON) {
        if (state == GLUT_UP) {
            if (dragging_index == NO_DRAG) {
                points.push_back({x, y});
                glutPostRedisplay();
            } else {
                points[dragging_index] = {x, y};
                dragging_index = NO_DRAG;
                glutPostRedisplay();
            }
        }
    }
}

void mouseMoved(int x, int y)
{
    if (dragging_index != -1) {
        points[dragging_index] = {x, y};
        glutPostRedisplay();
    }
}

void renderPolygon()
{
    glBegin(GL_LINE_STRIP);
    for (const Point& p : points) {
        glVertex2i(p.x, p.y);
    }
    glEnd();
}

#define DISTANCE 15
#define CLOSE(p, a_x, a_y) \
    ((p.x - a_x)*(p.x - a_x) <= DISTANCE*DISTANCE \
    && (p.y - a_y)*(p.y - a_y) <= DISTANCE*DISTANCE)
int findClosestPoint(int x, int y)
{
    for (int i = 0; i < points.size(); i ++) {
        const Point& point = points[i];
        if (CLOSE(point, x, y)) {
            return i;
        }
    }
    return NO_DRAG;
}
#undef CLOSE
#undef DISTANCE

void computeFactors(std::vector<int>& factors)
{
    int n = factors.size()-1;
    int a = 1;
    for (int i = 1; i <= n+1; i ++) {
        factors[i-1] = a;
        a = a * (n-i+1) / i;
    }
}

void drawBezier(std::vector<Point>& points)
{
    int pointCount = points.size();
    if (pointCount < 3) return;

    int n = pointCount-1;
    std::vector<int> factors(pointCount);
    factors[0] = 0;
    double t, b;

    computeFactors(factors);
    glBegin(GL_LINE_STRIP);
    for (int i = 0; i <= divs; i ++) {
        t = 1.0 / divs * i;
        Point point = {0, 0};
        for (int j = 0; j <= n; j ++) {
            if (j == 0) {
                b = factors[j]*pow(1-t, n);
            } else if (j == n) {
                b = factors[j]*pow(t, n);
            } else {
                b = factors[j]*pow(t, j)*pow(1-t, n-j);
            }
            point.x += b*points[j].x;
            point.y += b*points[j].y;
        }
        glVertex2i(point.x, point.y);
    }
    glEnd();
}

int factorial(int n)
{
    if (n <= 1) return 1;
    return factorial(n-1)*n;
}

double bezierWeight(int i, int n, double t)
{
    if ((i == 0 && n == 0) || i < 0) return 1;
    if (i > n) return 0;
    return (1-t)*bezierWeight(i, n-1, t) + t*bezierWeight(i-1, n-1, t);
}

void computeInterpolated()
{
    interpolated.clear();
    int pointCount = points.size();
    if (pointCount < 3) return;

    Matrix function_matrix(pointCount, pointCount);
    int n = pointCount-1;
    for (int row = 0; row < pointCount; row ++) {
        for (int col = 0; col < pointCount; col ++) {
            double value = bezierWeight(col, n, (double) row/n);
            function_matrix.Set(row, col, value);
        }
    }

    Matrix point_matrix(pointCount, 2);
    for (int row = 0; row < pointCount; row ++) {
        point_matrix.Set(row, 0, points[row].x);
        point_matrix.Set(row, 1, points[row].y);
    }

    std::unique_ptr<IMatrix> result = function_matrix.n_Invert()
            ->n_Multiply(point_matrix);
    for (int row = 0; row < pointCount; row ++) {
        interpolated.push_back({
            (int) result->Get(row, 0),
            (int) result->Get(row, 1)
        });
    }

    // Calculate exact points by adding the vectors.
    for (int i = 1; i < pointCount; i ++) {
        interpolated[i].x += interpolated[i-1].x;
        interpolated[i].y += interpolated[i-1].y;
    }
}
