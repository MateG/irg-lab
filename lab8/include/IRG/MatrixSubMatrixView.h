#ifndef MATRIXSUBMATRIXVIEW_H
#define MATRIXSUBMATRIXVIEW_H

#include "AbstractMatrix.h"

class MatrixSubMatrixView : public AbstractMatrix
{
private:
    IMatrix& original_;
    std::vector<int> row_indexes_;
    std::vector<int> col_indexes_;

    MatrixSubMatrixView(IMatrix& original,
            std::vector<int>& row_indexes, std::vector<int>& col_indexes);
public:
    MatrixSubMatrixView(IMatrix& original, int row, int col);
    virtual ~MatrixSubMatrixView();

    int GetRowsCount() const override;
    int GetColsCount() const override;
    double Get(int row, int col) const override;
    IMatrix& Set(int row, int col, double value) override;
    std::unique_ptr<IMatrix> Copy() const override;
    std::unique_ptr<IMatrix> NewInstance(int rows, int cols) const override;
    std::unique_ptr<IMatrix> SubMatrix(int row, int col, bool live_view) override;
};

#endif // MATRIXSUBMATRIXVIEW_H
