#ifndef IFS_H
#define IFS_H

#include <vector>
#include <fstream>
#include <sstream>

struct LineValues {
    double a, b, c, d, e, f, p;
};

struct IFS {
    int point_count;
    int limit;
    int eta_1, eta_2, eta_3, eta_4;
    std::vector<LineValues> lines;
};

#define ERR(m) throw std::runtime_error(m)
IFS loadIFS(const std::string& path) {
    std::ifstream file_stream(path);
    if (!file_stream.good()) ERR("File not found.");

    IFS ifs;
    ifs.lines = std::vector<LineValues>();
    int counter = 0;

    std::string type;
    std::string line;
    while (std::getline(file_stream, line)) {
        std::istringstream iss(line);
        if (!(iss >> type)) continue;
        if (type == "#") continue;

        iss = std::istringstream(line);
        if (counter == 0) {
            if (!(iss >> ifs.point_count)) ERR("Invalid point count.");
            counter ++;
        } else if (counter == 1) {
            if (!(iss >> ifs.limit)) ERR("Invalid limit.");
            counter ++;
        } else if (counter == 2) {
            if (!(iss >> ifs.eta_1)) ERR("Invalid eta 1.");
            if (!(iss >> ifs.eta_2)) ERR("Invalid eta 2.");
            counter ++;
        } else if (counter == 3) {
            if (!(iss >> ifs.eta_3)) ERR("Invalid eta 3.");
            if (!(iss >> ifs.eta_4)) ERR("Invalid eta 4.");
            counter ++;
        } else {
            LineValues line_values;
            if (!(iss >> line_values.a)) ERR("Invalid a.");
            if (!(iss >> line_values.b)) ERR("Invalid b.");
            if (!(iss >> line_values.c)) ERR("Invalid c.");
            if (!(iss >> line_values.d)) ERR("Invalid d.");
            if (!(iss >> line_values.e)) ERR("Invalid e.");
            if (!(iss >> line_values.f)) ERR("Invalid f.");
            if (!(iss >> line_values.p)) ERR("Invalid probability.");
            ifs.lines.push_back(line_values);
        }
    }
    return ifs;
}

#endif //IFS_H
