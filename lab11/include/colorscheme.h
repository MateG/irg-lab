#ifndef COLORSCHEME_H
#define COLORSCHEME_H

#include <GL/gl.h>

extern int max_limit;

void colorScheme1(int n) {
    if (n == -1) glColor3f(0.0f, 0.0f, 0.0f);
    else glColor3f(1.0f, 1.0f, 1.0f);
}

void colorScheme2(int n) {
    if (n == -1) {
        glColor3f(0.0f, 0.0f, 0.0f);
    } else if (max_limit < 16) {
        int r = (int) ((n-1) / (double) (max_limit-1)*255 + 0.5);
        int g = 255 - r;
        int b = ((n-1) % (max_limit/2))*255 / (max_limit/2);
        glColor3f(r/255.f, g/255.f, b/255.f);
    } else {
        int lim = max_limit < 32 ? max_limit : 32;
        int r = (n-1)*255 / lim;
        int g = ((n-1) % (lim/4))*255 / (lim/4);
        int b = ((n-1) % (lim/8))*255 / (lim/8);
        glColor3f(r/255.f, g/255.f, b/255.f);
    }
}

#endif //COLORSCHEME_H
