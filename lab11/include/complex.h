#ifndef COMPLEX_H
#define COMPLEX_H

struct Complex {
    double re;
    double im;
};

// x^2 - y^2 + i*2xy
int divergenceTest(Complex& c, int limit) {
    Complex z = {0.0, 0.0};
    for (int i = 1; i <= limit; i ++) {
        double next_re = z.re*z.re - z.im*z.im + c.re;
        double next_im = 2*z.re*z.im + c.im;
        z = {next_re, next_im};
        double square = z.re*z.re + z.im*z.im;
        if (square > 4) return i;
    }
    return -1;
}

// x^3 - xy^2 - 2xy^2 + i*(x^2y - y^3 + 2x^2y)
int divergenceTest2(Complex& c, int limit) {
    Complex z = {0.0, 0.0};
    for (int i = 1; i <= limit; i ++) {
        double next_re = z.re*z.re*z.re - 2*z.re*z.im*z.im + c.re;
        double next_im = z.re*z.re*z.im - z.im*z.im*z.im + 2*z.re*z.re*z.im + c.im;
        z = {next_re, next_im};
        double square = z.re*z.re + z.im*z.im;
        if (square > 4) return i;
    }
    return -1;
}

#endif //COMPLEX_H
