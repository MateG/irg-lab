#include <iostream>
#include <random>
#include <GL/glut.h>
#include "ifs.h"

void reshape(int width, int height);
void display();
void renderScene();

void setNextValues(double& x, double& y, double x0, double y0, double p);

IFS ifs;

std::mt19937 rng;
std::uniform_real_distribution<> distribution;

int main(int argc, char** argv) {
    if (argc != 2) {
        std::cerr << "Expected 1 argument - path to the IFS definition file." << std::endl;
        return 1;
    }

    ifs = loadIFS(argv[1]);
    std::random_device random_device;
    rng = std::mt19937(random_device());
    distribution = std::uniform_real_distribution<>(0.0, 1.0);

    glutInit(&argc, argv);
    glutInitWindowSize(600, 600);
    glutInitWindowPosition(400, 100);
    glutCreateWindow("Task 2");

    glutDisplayFunc(display);
    glutReshapeFunc(reshape);

    glutMainLoop();
    return 0;
}

void reshape(int width, int height) {
    glDisable(GL_DEPTH_TEST);
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, width, 0, height, 0, 1);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void display() {
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    renderScene();
    glutSwapBuffers();
}

void renderScene() {
    glColor3f(0.0f, 0.7f, 0.3f);
    glBegin(GL_POINTS);
    double x0, y0;
    for (int counter = 0; counter < ifs.point_count; counter ++) {
        x0 = 0.0;
        y0 = 0.0;
        for (int iter = 0; iter < ifs.limit; iter ++) {
            double x, y;
            double p = distribution(rng);
            setNextValues(x, y, x0, y0, p);
            x0 = x;
            y0 = y;
        }
        glVertex2i(
                (int) std::round(ifs.eta_1*x0 + ifs.eta_2),
                (int) std::round(ifs.eta_3*y0 + ifs.eta_4)
        );
    }
    glEnd();
}

void setNextValues(double& x, double& y, double x0, double y0, double p) {
    for (LineValues& line : ifs.lines) {
        if (p < line.p) {
            x = line.a*x0 + line.b*y0 + line.e;
            y = line.c*x0 + line.d*y0 + line.f;
            return;
        }
        p -= line.p;
    }

    LineValues& last = ifs.lines[ifs.lines.size()-1];
    x = last.a*x0 + last.b*y0 + last.e;
    y = last.c*x0 + last.d*y0 + last.f;
}