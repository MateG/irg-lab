#include <iostream>
#include <vector>
#include <GL/glut.h>
#include "complex.h"
#include "colorscheme.h"

void reshape(int width, int height);
void display();
void renderScene();

void keyPressed(unsigned char key, int x, int y);
void mousePressedOrReleased(int button, int state, int x, int y);

struct Area {
    double u_min, u_max, v_min, v_max;
};

double transformX(int x, Area &area);
double transformY(int y, Area &area);
Complex transform(int x, int y, Area &area);
Area toArea(int x, int y);

typedef int (*DT_PTRFUN)(Complex&, int);
DT_PTRFUN divergence_test_strategy;

typedef void (*CS_PTRFUN)(int);
CS_PTRFUN color_scheme_strategy;

std::vector<Area> area_stack;

int current_width = 800;
int current_height = 600;

int max_limit = 128;

int main(int argc, char** argv) {
    glutInit(&argc, argv);
    glutInitWindowSize(current_width, current_height);
    glutInitWindowPosition(400, 100);
    glutCreateWindow("Task 1");

    area_stack.push_back({-2.0, 1.0, -1.2, 1.2});
    divergence_test_strategy = divergenceTest;
    color_scheme_strategy = colorScheme2;

    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutKeyboardFunc(keyPressed);
    glutMouseFunc(mousePressedOrReleased);

    glutMainLoop();
    return 0;
}

void reshape(int width, int height) {
    current_width = width;
    current_height = height;

    glDisable(GL_DEPTH_TEST);
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, width, height, 0, 0, 1);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void display() {
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    renderScene();
    glutSwapBuffers();
}

void renderScene() {
    Area area = area_stack[area_stack.size()-1];
    glBegin(GL_POINTS);
    for (int y = 0; y < current_height; y ++) {
        for (int x = 0; x < current_width; x ++) {
            Complex c = transform(x, y, area);
            int n = divergence_test_strategy(c, max_limit);
            color_scheme_strategy(n);
            glVertex2i(x, y);
        }
    }
    glEnd();
}

double transformX(int x, Area &area) {
    return (double) x/current_width*(area.u_max-area.u_min)+area.u_min;
}

double transformY(int y, Area &area) {
    return (double) y/current_height*(area.v_max-area.v_min)+area.v_min;
}

Complex transform(int x, int y, Area &area) {
    return {transformX(x, area), transformY(y, area)};
}

void keyPressed(unsigned char key, int x, int y) {
    if (key == '1') {
        divergence_test_strategy = divergenceTest;
    } else if (key == '2') {
        divergence_test_strategy = divergenceTest2;
    } else if (key == 'b') {
        color_scheme_strategy = colorScheme1;
    } else if (key == 'c') {
        color_scheme_strategy = colorScheme2;
    } else if (key == 'x') {
        if (area_stack.size() > 1) {
            area_stack.pop_back();
        }
    } else if (key == 27) {
        while (area_stack.size() > 1) {
            area_stack.pop_back();
        }
    }

    glutPostRedisplay();
}

void mousePressedOrReleased(int button, int state, int x, int y) {
    if (button == GLUT_LEFT_BUTTON) {
        if (state == GLUT_DOWN) {
            area_stack.push_back(toArea(x, y));
            glutPostRedisplay();
        }
    }
}

Area toArea(int x, int y) {
    Area current = area_stack[area_stack.size()-1];
    double translated_x = transformX(x, current);
    double translated_y = transformY(y, current);
    double width = current.u_max - current.u_min;
    double new_half_width = (width/16.0)/2.0;
    double height = current.v_max - current.v_min;
    double new_half_height = (height/16.0)/2.0;
    return {
        translated_x - new_half_width,
        translated_x + new_half_width,
        translated_y - new_half_height,
        translated_y + new_half_height,
    };
}