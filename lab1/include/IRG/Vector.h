#ifndef VECTOR_H
#define VECTOR_H

#include "AbstractVector.h"

#define IRG_UNIQUE_VECTOR(x, ...) \
    (std::make_unique<Vector>(std::initializer_list<double>({x, __VA_ARGS__})))

class Vector : public AbstractVector
{
private:
    std::unique_ptr<std::vector<double>> elements_;
    bool read_only_;
public:
    Vector(const Vector& vector);
    Vector(std::vector<double>& elements);
    Vector(bool immutable, bool store_reference, std::vector<double>& elements);
    Vector(const std::initializer_list<double>& list);
    ~Vector();

    double Get(int index) const override;
    IVector& Set(int index, double value) override;
    int GetDimension() const override;
    std::unique_ptr<IVector> Copy() const override;
    std::unique_ptr<IVector> NewInstance(int dimension) const override;

    static std::unique_ptr<Vector> ParseSimple(const std::string& text);
};

#endif // VECTOR_H
