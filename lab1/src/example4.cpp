#include <iostream>
#include <iomanip>
#include <IRG/Vector.h>

bool checkDimension(const IVector& vector)
{
    int dimension = vector.GetDimension();
    if (dimension < 2 || dimension > 3) {
        std::cerr << "Only 2D and 3D vectors allowed!" << std::endl;
        return false;
    }
    return true;
}

bool checkNorm(double norm)
{
    if (norm == 0.0) {
        std::cerr << "Null vectors not allowed!" << std::endl;
        return false;
    }
    return true;
}

int main(void)
{
    std::cout << "Vector m: ";
    std::string input;
    std::getline(std::cin, input);
    std::unique_ptr<IVector> m = Vector::ParseSimple(input);
    if (!checkDimension(*m)) return 1;
    double m_norm = m->Norm();
    if (!checkNorm(m_norm)) return 1;

    std::cout << "Vector n: ";
    std::getline(std::cin, input);
    std::unique_ptr<IVector> n = Vector::ParseSimple(input);
    if (!checkDimension(*n)) return 1;
    double n_norm = n->Norm();
    if (!checkNorm(n_norm)) return 1;

    if (m_norm == 0 || n_norm == 0) {
        std::cerr << "Null vectors not allowed!" << std::endl;
        return 1;
    }
    double cos_alpha = m->ScalarProduct(*n) / (m_norm * n_norm);

    std::unique_ptr<IVector> k = n->n_ScalarMultiply(m_norm * cos_alpha / n_norm);
    std::unique_ptr<IVector> r = k->n_ScalarMultiply(2);
    r->Sub(*m);

    std::cout << std::setprecision(3) << *r << std::endl;
    return 0;
}
