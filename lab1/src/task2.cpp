#include <iostream>
#include <iomanip>
#include <IRG/Matrix.h>
#include <IRG/VectorMatrixView.h>
#include <IRG/IrgException.h>

// 1 1 1 6 -1 -2 1 -2 2 1 3 13
int main(void)
{
    const int input_count = 12;
    double inputs[input_count];
    std::cout << "Coefficients and constant terms of the 3-equation linear system:" << std::endl;
    for (int i = 0; i < input_count; i ++) {
        std::cin >> inputs[i];
    }

    Matrix coefficients = {
        {inputs[0], inputs[1], inputs[2]},
        {inputs[4], inputs[5], inputs[6]},
        {inputs[8], inputs[9], inputs[10]}
    };
    Matrix constants = {{inputs[3]}, {inputs[7]}, {inputs[11]}};

    try {
        std::unique_ptr<IMatrix> solution = coefficients.n_Invert()->n_Multiply(constants);
        VectorMatrixView vector_view(*solution);
        std::cout << "[x, y, z] = " << std::setprecision(3) << vector_view << std::endl;
    } catch (IrgException& ex) {
        std::cerr << "Invalid linear system!" << std::endl;
    }
    return 0;
}
