#include <iostream>
#include <iomanip>
#include <IRG/Vector.h>
#include <IRG/Matrix.h>

std::unique_ptr<IVector> calculateV1()
{
    Vector a = {2, 3, -4};
    Vector b = {-1, 4, -3};
    std::unique_ptr<IVector> v1 = a.n_Add(b);
    std::cout << "v1 = " << a << " + " << b << " = " << *v1 << std::endl;
    return v1;
}

void calculateS(const IVector& v1)
{
    Vector a = {-1, 4, -3};
    double s = v1.ScalarProduct(a);
    std::cout << "s  = " << v1 << " * " << a << " = " << s << std::endl;
}

std::unique_ptr<IVector> calculateV2(const IVector& v1)
{
    Vector a = {2, 2, 4};
    std::unique_ptr<IVector> v2 = v1.n_VectorProduct(a);
    std::cout << "v2 = " << v1 << " x " << a << " = " << *v2 << std::endl;
    return v2;
}

void calculateV3(const IVector& v2)
{
    std::unique_ptr<IVector> v3 = v2.n_Normalize();
    std::cout << "v3 = normalize(" << v2 << ") = "
        << std::setprecision(3) << *v3 << std::endl;
}

void calculateV4(const IVector& v2)
{
    std::unique_ptr<IVector> v4 = v2.n_ScalarMultiply(-1.0);
    std::cout << "v4 = -(" << v2 << ") = " << *v4 << std::endl;
}

void calculateM1()
{
    Matrix a = {{1, 2, 3}, {2, 1, 3}, {4, 5, 1}};
    Matrix b = {{-1, 2, -3}, {5, -2, 7}, {-4, -1, 3}};
    std::cout << "M1" << std::endl
            << '=' << std::endl
            << a << std::endl
            << '+' << std::endl
            << b << std::endl
            << '=' << std::endl
            << a.Add(b) << std::endl;
}

void calculateM2()
{
    Matrix a = {{1, 2, 3}, {2, 1, 3}, {4, 5, 1}};
    Matrix b = {{-1, 2, -3}, {5, -2, 7}, {-4, -1, 3}};
    std::cout << "M2" << std::endl
            << '=' << std::endl
            << a << std::endl
            << '*' << std::endl
            << b << "^T" << std::endl
            << '=' << std::endl
            << *(a.n_Multiply(*(b.n_Transpose(true)))) << std::endl;
}

void calculateM3()
{
    Matrix a = {{-24, 18, 5}, {20, -15, -4}, {-5, 4, 1}};
    Matrix b = {{1, 2, 3}, {0, 1, 4}, {5, 6, 0}};
    std::cout << "M3" << std::endl
            << '=' << std::endl
            << a << "^-1" << std::endl
            << '*' << std::endl
            << b << "^-1" << std::endl
            << '=' << std::endl
            << *(a.n_Invert()->n_Multiply(*b.n_Invert())) << std::endl;
}

int main(void)
{
    std::cout << std::setprecision(0);
    std::unique_ptr<IVector> v1 = calculateV1();
    calculateS(*v1);
    std::unique_ptr<IVector> v2 = calculateV2(*v1);
    calculateV3(*v2);
    std::cout << std::setprecision(0);
    calculateV4(*v2);

    std::cout << std::endl;
    calculateM1();
    std::cout << std::endl;
    calculateM2();
    std::cout << std::endl;
    calculateM3();

    return 0;
}
