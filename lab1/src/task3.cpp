#include <iostream>
#include <iomanip>
#include <IRG/Matrix.h>
#include <IRG/VectorMatrixView.h>
#include <IRG/IrgException.h>

int main(void)
{
    std::cout << "Please enter x, y and z coordinates of the following points." << std::endl;
    const int count = 3;
    Matrix abc(count, count);
    double value;

    std::cout << "A: ";
    for (int i = 0; i < count; i ++) {
        std::cin >> value;
        abc.Set(0, i, value);
    }

    std::cout << "B: ";
    for (int i = 0; i < count; i ++) {
        std::cin >> value;
        abc.Set(1, i, value);
    }

    std::cout << "C: ";
    for (int i = 0; i < count; i ++) {
        std::cin >> value;
        abc.Set(2, i, value);
    }

    Matrix t(3, 1);
    std::cout << "T: ";
    for (int i = 0; i < count; i ++) {
        std::cin >> value;
        t.Set(i, 0, value);
    }

    /*for (int i = 0; i < count; i ++) {
        if (!abc.Get(i, 0) && !abc.Get(i, 1) && !abc.Get(i, 2)) {
            abc.Set(i, 0, 1.0);
            abc.Set(i, 1, 1.0);
            abc.Set(i, 2, 1.0);
            t.Set(i, 0, 1.0);
            break;
        }
    }*/

    try {
        std::unique_ptr<IMatrix> solution = abc.n_Invert()->n_Multiply(t);
        VectorMatrixView vector_view(*solution);
        std::cout << std::setprecision(3) << std::endl;
        std::cout << "Baricentric coordinates are: " << vector_view << std::endl;
    } catch (IrgException& ex) {
        std::cerr << "Invalid coordinates!" << std::endl;
    }
    return 0;
}
