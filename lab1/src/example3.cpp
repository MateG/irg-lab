#include <iostream>
#include <iomanip>
#include <IRG/Matrix.h>

int main(void)
{
    std::unique_ptr<IMatrix> singular = IRG_UNIQUE_MATRIX({1, 5, 3}, {0, 0, 8}, {0, 0, 0});
    std::cout << std::setprecision(3);
    std::cout << "Singularna matrica:" << std::endl;
    std::cout << *singular << std::endl;
    std::cout << "Determinanta: " << singular->Determinant() << std::endl << std::endl;

    std::unique_ptr<IMatrix> a = IRG_UNIQUE_MATRIX({1, 5, 3}, {0, 0, 8}, {1, 1, 1});
    std::unique_ptr<IMatrix> r = IRG_UNIQUE_MATRIX({3}, {4}, {1});
    std::unique_ptr<IMatrix> baricentric = a->n_Invert()->n_Multiply(*r);
    std::cout << "Baricentricne koordinate:" << std::endl;
    std::cout << *baricentric << std::endl;
    return 0;
}
