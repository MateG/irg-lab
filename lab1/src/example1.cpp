#include <iostream>
#include <IRG/Vector.h>

int main(void) {
    std::unique_ptr<IVector> a = Vector::ParseSimple("1 0 0");
    std::unique_ptr<IVector> b = Vector::ParseSimple("5 0 0");
    std::unique_ptr<IVector> c = Vector::ParseSimple("3 8 0");

    std::unique_ptr<IVector> t = Vector::ParseSimple("3 4 0");

    double pov = b->n_Sub(*a)->n_VectorProduct(*c->n_Sub(*a))->Norm() / 2.0;
    double povA = b->n_Sub(*t)->n_VectorProduct(*c->n_Sub(*t))->Norm() / 2.0;
    double povB = a->n_Sub(*t)->n_VectorProduct(*c->n_Sub(*t))->Norm() / 2.0;
    double povC = a->n_Sub(*t)->n_VectorProduct(*b->n_Sub(*t))->Norm() / 2.0;

    double t1 = povA / pov;
    double t2 = povB / pov;
    double t3 = povC / pov;

    std::cout << "Baricentricne koordinate su: (" << t1 << ',' << t2 << ',' << t3 << ")." << std::endl;
    return 0;
}
