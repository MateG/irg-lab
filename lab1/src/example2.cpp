#include <iostream>
#include <iomanip>
#include <IRG/Matrix.h>

int main(void)
{
    std::unique_ptr<IMatrix> a = Matrix::ParseSimple("3 5 | 2 10");
    std::unique_ptr<IMatrix> r = Matrix::ParseSimple("2 | 8");
    std::unique_ptr<IMatrix> v = a->n_Invert()->n_Multiply(*r);

    std::cout << std::setprecision(3);
    std::cout << "Rjesenje sustava je:" << std::endl;
    std::cout << *v << std::endl;
    return 0;
}
