#include "common.h"
#include "IRG/Matrix.h"
#include "IRG/Vector.h"
#include "IRG/IRG.h"

Vector eye = {3.0, 4.0, 1.0};
Vector center = {0.0, 0.0, 0.0};
Vector view_up = {0.0, 1.0, 0.0};
std::unique_ptr<IMatrix> look_at = irg::LookAtMatrix(eye, center, view_up);
std::unique_ptr<IMatrix> frustum = irg::BuildFrustumMatrix(-0.5, 0.5, -0.5, 0.5, 1, 100);
std::unique_ptr<IMatrix> m = look_at->n_Multiply(*frustum);

int main(int argc, char** argv)
{
    launch(argc, argv, "Task 3");
    return 0;
}

void reshape(int width, int height)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glViewport(0, 0, width, height);
}

void display()
{
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    renderScene();
    glutSwapBuffers();
}

void renderScene()
{
    glColor3f(1.0f, 0.0f, 0.0f);
    std::vector<Vertex3D>& vertices = model->GetVertices();
    for (const Face3D& triangle : model->GetTriangles()) {
        glBegin(GL_LINE_LOOP);
        for (int i = 0; i < 3; i ++) {
            Vertex3D& vertex = vertices[triangle.indexes[i]];
            Vector v = {vertex.x, vertex.y, vertex.z, 1.0};
            std::unique_ptr<IVector> tv = v.ToRowMatrix(false)->n_Multiply(*m)
                                            ->ToVector(false)->n_FromHomogenous();
            glVertex2f((float) tv->Get(0), (float) tv->Get(1));
        }
        glEnd();
    }
}

void updateEye(double x, double z)
{
    eye.Set(0, x);
    eye.Set(2, z);
    look_at = irg::LookAtMatrix(eye, center, view_up);
    m = look_at->n_Multiply(*frustum);
}
