#include <GL/glut.h>

void reshape(int width, int height);
void display();
void renderScene();

int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE);
    glutInitWindowSize(640, 480);
    glutInitWindowPosition(200, 100);
    glutCreateWindow("Example");

    glutDisplayFunc(display);
    glutReshapeFunc(reshape);

    glutMainLoop();
    return 0;
}

void reshape(int width, int height)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glViewport(0, 0, width, height);
    //glViewport(width/2, height/2, width/2, height/2);
    //glViewport(0, 0, width/2, height/2);
    //glViewport(width/4, height/4, width/2, height/2);
}

void display()
{
    glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    renderScene();
    glutSwapBuffers();
}

void renderScene()
{
    glColor3f(1.0f, 0.0f, 0.0f);
    glBegin(GL_LINE_STRIP);
    glVertex3f(-0.9f, -0.9f, -0.9f);
    glVertex3f(0.9f, -0.9f, -0.9f);
    glEnd();

    glColor3f(1.0f, 0.0f, 0.0f);
    glBegin(GL_LINE_STRIP);
    glVertex3f(-0.9f, -0.7f, -0.9f);
    glVertex3f(0.9f, -0.7f, 3.1f);
    glEnd();
}
