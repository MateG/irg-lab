#include "common.h"

double eye_x = 3.0, eye_z = 1.0;

int main(int argc, char** argv)
{
    launch(argc, argv, "Task 2");
    return 0;
}

void reshape(int width, int height)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    // h          = 2 * near * tg(fovy/2)
    // tg(fovy/2) = 0.5
    // fovy       = arctg(0.5)*2 * 180/pi
    gluPerspective(53.1301023542, 1.0, 1.0, 100.0);
    glViewport(0, 0, width, height);
}

void display()
{
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(
        eye_x, 4.0, eye_z,
        0.0, 0.0, 0.0,
        0.0, 1.0, 0.0
    );
    renderScene();
    glutSwapBuffers();
}

void renderScene()
{
    glColor3f(1.0f, 0.0f, 0.0f);
    std::vector<Vertex3D>& vertices = model->GetVertices();
    for (const Face3D& triangle : model->GetTriangles()) {
        glBegin(GL_LINE_LOOP);
        for (int i = 0; i < 3; i ++) {
            Vertex3D& v = vertices[triangle.indexes[i]];
            glVertex3f(v.x, v.y, v.z);
        }
        glEnd();
    }
}

void updateEye(double x, double z)
{
    eye_x = x;
    eye_z = z;
}
