#include "ObjectModel.h"

#include <algorithm>
#include <fstream>
#include <sstream>

ObjectModel::ObjectModel(std::vector<Vertex3D>& vertices, std::vector<Face3D>& triangles)
: vertices_(vertices), triangles_(triangles)
{
}

ObjectModel::~ObjectModel()
{
}

std::vector<Vertex3D>& ObjectModel::GetVertices()
{
    return vertices_;
}

std::vector<Face3D>& ObjectModel::GetTriangles()
{
    return triangles_;
}

ObjectModel ObjectModel::Copy() const
{
    std::vector<Vertex3D> vertices = vertices_;
    std::vector<Face3D> triangles = triangles_;
    return ObjectModel(vertices, triangles);
}

#define CENTER(min, max) ((min+max) / 2)
void ObjectModel::Normalize()
{
    double x_min, x_max, y_min, y_max, z_min, z_max;
    x_min = x_max = vertices_[0].x;
    y_min = y_max = vertices_[0].y;
    z_min = z_max = vertices_[0].z;
    for (int i = 1; i < (int) vertices_.size(); i ++) {
        const Vertex3D& vertex = vertices_[i];
        if (vertex.x < x_min) x_min = vertex.x;
        else if (vertex.x > x_max) x_max = vertex.x;
        if (vertex.y < y_min) y_min = vertex.y;
        else if (vertex.y > y_max) y_max = vertex.y;
        if (vertex.z < z_min) z_min = vertex.z;
        else if (vertex.z > z_max) z_max = vertex.z;
    }

    double x_center = CENTER(x_min, x_max);
    double y_center = CENTER(y_min, y_max);
    double z_center = CENTER(z_min, z_max);
    for (Vertex3D& vertex : vertices_) {
        vertex.x -= x_center;
        vertex.y -= y_center;
        vertex.z -= z_center;
    }

    double max_range = std::max({x_max-x_min, y_max-y_min, z_max-z_min});
    double scale = 2.0 / max_range;
    for (Vertex3D& vertex : vertices_) {
        vertex.x *= scale;
        vertex.y *= scale;
        vertex.z *= scale;
    }
}
#undef CENTER

std::unique_ptr<ObjectModel> ObjectModel::ParseFromFile(const std::string& path)
{
    std::vector<Vertex3D> vertices;
    std::vector<Face3D> triangles;

    std::ifstream file_stream(path);
    std::string line;
    char type;
    while (std::getline(file_stream, line)) {
        std::istringstream iss(line);
        if (!(iss >> type)) continue;
        if (type == 'v') {
            double x, y, z;
            if (!(iss >> x)) continue;
            if (!(iss >> y)) continue;
            if (!(iss >> z)) continue;
            vertices.push_back({x, y, z});
        } else if (type == 'f') {
            int i0, i1, i2;
            if (!(iss >> i0)) continue;
            if (!(iss >> i1)) continue;
            if (!(iss >> i2)) continue;
            triangles.push_back({{i0-1, i1-1, i2-1}});
        }
    }
    return std::unique_ptr<ObjectModel>(new ObjectModel(vertices, triangles));
}

std::ostream& operator<<(std::ostream& os, const ObjectModel& model)
{
    for (const Vertex3D& vertex : model.vertices_) {
        os << "v " << vertex.x << ' ' << vertex.y << ' ' << vertex.z << '\n';
    }
    for (const Face3D& triangle : model.triangles_) {
        os << 'f';
        for (int i = 0; i < 3; i ++) {
            os << ' ' << triangle.indexes[i] + 1;
        }
        os << '\n';
    }
    return os;
}
