#ifndef IMATRIX_H
#define IMATRIX_H

#include <memory>
#include <vector>
class IVector;

class IMatrix
{
private:
    virtual std::ostream& print(std::ostream& os) const = 0;
public:
    virtual ~IMatrix();

    virtual int GetRowsCount() const = 0;
    virtual int GetColsCount() const = 0;
    virtual double Get(int row, int col) const = 0;
    virtual IMatrix& Set(int row, int col, double value) = 0;
    virtual std::unique_ptr<IMatrix> Copy() const = 0;
    virtual std::unique_ptr<IMatrix> NewInstance(int rows, int cols) const = 0;
    virtual std::unique_ptr<IMatrix> n_Transpose(bool live_view) = 0;
    virtual IMatrix& Add(const IMatrix& other) = 0;
    virtual std::unique_ptr<IMatrix> n_Add(const IMatrix& other) const = 0;
    virtual IMatrix& Sub(const IMatrix& other) = 0;
    virtual std::unique_ptr<IMatrix> n_Sub(const IMatrix& other) const = 0;
    virtual std::unique_ptr<IMatrix> n_Multiply(const IMatrix& other) const = 0;
    virtual double Determinant() = 0;
    virtual std::unique_ptr<IMatrix> SubMatrix(int row, int col, bool live_view) = 0;
    virtual std::unique_ptr<IMatrix> n_Invert() = 0;
    virtual std::vector<std::vector<double>> ToArray() const = 0;
    virtual std::unique_ptr<IVector> ToVector(bool live_view) = 0;

    friend std::ostream& operator<<(std::ostream& os, const IMatrix& matrix);
};

#endif // IMATRIX_H
