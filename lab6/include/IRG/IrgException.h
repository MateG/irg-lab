#ifndef IRGEXCEPTION_H
#define IRGEXCEPTION_H

#include <stdexcept>

class IrgException : public std::runtime_error
{
public:
    IrgException(const char* message);
};

#endif // IRGEXCEPTION_H
