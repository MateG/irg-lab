#ifndef BRESENHAM_H
#define BRESENHAM_H

void bresenhamPositive(int xs, int ys, int xe, int ye)
{
    int x, yc, correction, a, yf;
    if (ye - ys <= xe - xs) {
        a = 2*(ye - ys);
        yc = ys;
        yf = -(xe - xs);
        correction = -2*(xe - xs);
        for (x = xs; x <= xe; x ++) {
            glVertex2i(x, yc);
            yf += a;
            if (yf >= 0) {
                yf += correction;
                yc ++;
            }
        }
    } else {
        x = xe;
        xe = ye;
        ye = x;

        x = xs;
        xs = ys;
        ys = x;

        a = 2*(ye - ys);
        yc = ys;
        yf = -(xe - xs);
        correction = -2*(xe - xs);
        for (x = xs; x <= xe; x ++) {
            glVertex2i(yc, x);
            yf += a;
            if (yf >= 0) {
                yf += correction;
                yc ++;
            }
        }
    }
}

void bresenhamNegative(int xs, int ys, int xe, int ye)
{
    int x, yc, correction, a, yf;
    if (-(ye - ys) <= xe - xs) {
        a = 2*(ye - ys);
        yc = ys;
        yf = xe - xs;
        correction = 2*(xe - xs);
        for (x = xs; x <= xe; x ++) {
            glVertex2i(x, yc);
            yf += a;
            if (yf <= 0) {
                yf += correction;
                yc --;
            }
        }
    } else {
        x = xe;
        xe = ys;
        ys = x;

        x = xs;
        xs = ye;
        ye = x;

        a = 2*(ye - ys);
        yc = ys;
        yf = xe - xs;
        correction = 2*(xe - xs);
        for (x = xs; x <= xe; x ++) {
            glVertex2i(yc, x);
            yf += a;
            if (yf <= 0) {
                yf += correction;
                yc --;
            }
        }
    }
}

void bresenham(int xs, int ys, int xe, int ye)
{
    glBegin(GL_POINTS);
    if (xs <= xe) {
        if (ys <= ye) {
            bresenhamPositive(xs, ys, xe, ye); // I.
        } else {
            bresenhamNegative(xs, ys, xe, ye); // IV.
        }
    } else {
        if (ys >= ye) {
            bresenhamPositive(xe, ye, xs, ys); // III.
        } else {
            bresenhamNegative(xe, ye, xs, ys); // II.
        }
    }
    glEnd();
}

#endif // BRESENHAM_H
