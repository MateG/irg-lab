#include <cmath>
#include <vector>
#include <GL/glut.h>
#include "bresenham.h"

void reshape(int width, int height);
void display();
void renderScene();
void bresenhamCutoff(int xs, int ys, int xe, int ye);

void keyPressed(unsigned char key, int x, int y);
void mousePressedOrReleased(int button, int state, int x, int y);
void mouseMoved(int x, int y);

struct Line {
    int xs, ys, xe, ye;
};
std::vector<Line> lines;

int xs, ys, mouse_x, mouse_y;
bool started = false;

bool cutoff = false, control = false;
int cutoff_xs, cutoff_xe, cutoff_ys, cutoff_ye;

int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE);
    glutInitWindowSize(800, 600);
    glutInitWindowPosition(200, 200);
    glutCreateWindow("Example");

    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutKeyboardFunc(keyPressed);
    glutMouseFunc(mousePressedOrReleased);
    glutPassiveMotionFunc(mouseMoved);

    glutMainLoop();
    return 0;
}

void reshape(int width, int height)
{
    glDisable(GL_DEPTH_TEST);
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, width-1, height-1, 0, 0, 1);
    glMatrixMode(GL_MODELVIEW);

    cutoff_xs = width/4;
    cutoff_xe = width - cutoff_xs;
    cutoff_ys = height/4;
    cutoff_ye = height - cutoff_ys;
}

void display()
{
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    glLoadIdentity();
    renderScene();
    glutSwapBuffers();
}

void renderScene()
{
    if (cutoff) {
        glColor3f(0.0f, 1.0f, 0.0f);
        glBegin(GL_LINE_LOOP);
        glVertex2i(cutoff_xs, cutoff_ys);
        glVertex2i(cutoff_xe, cutoff_ys);
        glVertex2i(cutoff_xe, cutoff_ye);
        glVertex2i(cutoff_xs, cutoff_ye);
        glEnd();
    }

    glColor3f(0.0f, 0.0f, 0.0f);
    for (Line line : lines) {
        bresenhamCutoff(line.xs, line.ys, line.xe, line.ye);
    }
    if (started) {
        bresenhamCutoff(xs, ys, mouse_x, mouse_y);
    }

    if (control) {
        glColor3f(1.0f, 0.0f, 0.0f);
        glBegin(GL_LINES);
        for (Line line : lines) {
            double angle = -std::atan2(line.ye-line.ys, line.xe-line.xs);
            int delta_x = std::round(4*std::sin(angle));
            int delta_y = std::round(4*std::cos(angle));
            glVertex2i(line.xs + delta_x, line.ys + delta_y);
            glVertex2i(line.xe + delta_x, line.ye + delta_y);
        }
        glEnd();
    }
}

void bresenhamCutoff(int xs, int ys, int xe, int ye)
{
    if (cutoff) {
        if ((xs < cutoff_xs && xe < cutoff_xs)
                || (xs > cutoff_xe && xe > cutoff_xe)
                || (ys < cutoff_ys && ye < cutoff_ys)
                || (ys > cutoff_ye && ye > cutoff_ye)) {
            return;
        }

        if (xs == xe) {
            if (ys < cutoff_ys) ys = cutoff_ys;
            else if (ye < cutoff_ys) ye = cutoff_ys;
            if (ys > cutoff_ye) ys = cutoff_ye;
            else if (ye > cutoff_ye) ye = cutoff_ye;
        } else {
            double k = (double) (ye-ys) / (xe-xs);
            double l = ys - k*xs;

            if (xs < cutoff_xs) {
                xs = cutoff_xs;
                ys = std::round(k*xs + l);
                if ((ys < cutoff_ys && ye < cutoff_ys)
                        || (ys > cutoff_ye && ye > cutoff_ye)) {
                    return;
                }
            } else if (xe < cutoff_xs) {
                xe = cutoff_xs;
                ye = std::round(k*xe + l);
                if ((ys < cutoff_ys && ye < cutoff_ys)
                        || (ys > cutoff_ye && ye > cutoff_ye)) {
                    return;
                }
            }

            if (xs > cutoff_xe) {
                xs = cutoff_xe;
                ys = std::round(k*xs + l);
                if ((ys < cutoff_ys && ye < cutoff_ys)
                        || (ys > cutoff_ye && ye > cutoff_ye)) {
                    return;
                }
            } else if (xe > cutoff_xe) {
                xe = cutoff_xe;
                ye = std::round(k*xe + l);
                if ((ys < cutoff_ys && ye < cutoff_ys)
                        || (ys > cutoff_ye && ye > cutoff_ye)) {
                    return;
                }
            }

            if (ys < cutoff_ys) {
                ys = cutoff_ys;
                xs = std::round((ys-l) / k);
                if ((xs < cutoff_xs && xe < cutoff_xs)
                        || (xs > cutoff_xe && xe > cutoff_xe)) {
                    return;
                }
            } else if (ye < cutoff_ys) {
                ye = cutoff_ys;
                xe = std::round((ye-l) / k);
                if ((xs < cutoff_xs && xe < cutoff_xs)
                        || (xs > cutoff_xe && xe > cutoff_xe)) {
                    return;
                }
            }

            if (ys > cutoff_ye) {
                ys = cutoff_ye;
                xs = std::round((ys-l) / k);
                if ((xs < cutoff_xs && xe < cutoff_xs)
                        || (xs > cutoff_xe && xe > cutoff_xe)) {
                    return;
                }
            } else if (ye > cutoff_ye) {
                ye = cutoff_ye;
                xe = std::round((ye-l) / k);
                if ((xs < cutoff_xs && xe < cutoff_xs)
                        || (xs > cutoff_xe && xe > cutoff_xe)) {
                    return;
                }
            }
        }
    }

    bresenham(xs, ys, xe, ye);
}

void keyPressed(unsigned char key, int x, int y)
{
    if (key == 'o') {
        cutoff = !cutoff;
        glutPostRedisplay();
    } else if (key == 'k') {
        control = !control;
        glutPostRedisplay();
    }
}

void mousePressedOrReleased(int button, int state, int x, int y)
{
    if (button == GLUT_LEFT_BUTTON) {
        if (state == GLUT_UP) {
            if (!started) {
                xs = x;
                ys = y;
            } else {
                lines.push_back({xs, ys, x, y});
            }
            started = !started;
            glutPostRedisplay();
        }
    }
}

void mouseMoved(int x, int y)
{
    mouse_x = x;
    mouse_y = y;
    if (started) glutPostRedisplay();
}
