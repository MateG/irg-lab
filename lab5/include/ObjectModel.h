#ifndef OBJECTMODEL_H
#define OBJECTMODEL_H

#include <memory>
#include <vector>

struct Vertex3D {
    double x, y, z;
};

struct Face3D {
    int indexes[3];
};

class ObjectModel
{
private:
    std::vector<Vertex3D> vertices_;
    std::vector<Face3D> triangles_;

    ObjectModel(std::vector<Vertex3D>& vertices, std::vector<Face3D>& triangles);
public:
    virtual ~ObjectModel();

    std::vector<Vertex3D>& GetVertices();
    std::vector<Face3D>& GetTriangles();

    ObjectModel Copy() const;
    void Normalize();

    static std::unique_ptr<ObjectModel> ParseFromFile(const std::string& path);
    friend std::ostream& operator<<(std::ostream& os, const ObjectModel& model);
};

#endif // OBJECTMODEL_H
