#include <iostream>
#include <sstream>

#include "ObjectModel.h"

#define POINT_INSIDE 1
#define POINT_OUTSIDE -1
#define POINT_ON 0
int pointPolygonStatus(ObjectModel& model, double x, double y, double z);

int main(int argc, char** argv)
{
    if (argc != 2) {
        std::cerr << "Expected a single argument - path to the .obj file." << std::endl;
        return 1;
    }
    std::unique_ptr<ObjectModel> model = ObjectModel::ParseFromFile(argv[1]);

    const std::string QUIT_COMMAND = "quit";
    std::cout << "Quit command - " << QUIT_COMMAND << std::endl;
    const std::string NORM_COMMAND = "normiraj";
    std::cout << "Norm command - " << NORM_COMMAND << std::endl;
    std::cout << "Form of input - x y z" << std::endl << std::endl;
    const std::string INPUT_PROMPT = "> ";

    std::string input;
    while (true) {
        std::cout << INPUT_PROMPT;
        std::getline(std::cin, input);
        if (input == QUIT_COMMAND) break;
        else if (input == NORM_COMMAND) {
            ObjectModel copy = model->Copy();
            copy.Normalize();
            std::cout << copy << std::endl;
        } else {
            std::istringstream iss(input);
            double x, y, z;
            if (!(iss >> x) || !(iss >> y) || !(iss >> z)) {
                std::cout << "Invalid input!" << std::endl;
                continue;
            }

            int status = pointPolygonStatus(*model, x, y, z);
            std::cout << "Point (" << x << ", " << y << ", " << z << ')' << std::endl;
            std::cout << "Status: ";
            if (status == POINT_OUTSIDE) {
                std::cout << "OUTSIDE";
            } else if (status == POINT_INSIDE) {
                std::cout << "INSIDE";
            } else if (status == POINT_ON) {
                std::cout << "ON";
            }
            std::cout << std::endl;
        }
    }
    return 0;
}

int pointPolygonStatus(ObjectModel& model, double x, double y, double z)
{
    std::vector<Vertex3D>& vertices = model.GetVertices();
    int below = 0, on = 0;
    for (const Face3D& triangle : model.GetTriangles()) {
        const Vertex3D& a = vertices[triangle.indexes[0]];
        const Vertex3D& b = vertices[triangle.indexes[1]];
        const Vertex3D& c = vertices[triangle.indexes[2]];

        double ab_x = b.x - a.x;
        double ab_y = b.y - a.y;
        double ab_z = b.z - a.z;

        double ac_x = c.x - a.x;
        double ac_y = c.y - a.y;
        double ac_z = c.z - a.z;

        double n_x = ab_y*ac_z - ab_z*ac_y;
        double n_y = ab_z*ac_x - ab_x*ac_z;
        double n_z = ab_x*ac_y - ab_y*ac_x;

        double dot_product = (x-a.x)*n_x + (y-a.y)*n_y + (z-a.z)*n_z;
        if (dot_product < 0) below ++;
        else if (dot_product > 0) return POINT_OUTSIDE;
        else on ++;
    }

    return on == 0 ? POINT_INSIDE : POINT_ON;
}
