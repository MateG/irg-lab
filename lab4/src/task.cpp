#include <iostream>
#include <cmath>
#include <vector>
#include <GL/glut.h>
#include "polygon.h"

void reshape(int width, int height);
void display();
void renderScene();

void keyPressed(unsigned char key, int x, int y);
void mousePressedOrReleased(int button, int state, int x, int y);
void mouseMoved(int x, int y);

std::vector<PolygonElement> polygon = {{{0, 0}, {0, 0, 0}, false}};

bool filling = false, convexity = false, drawing_state = true;

int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE);
    glutInitWindowSize(800, 600);
    glutInitWindowPosition(200, 200);
    glutCreateWindow("Example");

    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutKeyboardFunc(keyPressed);
    glutMouseFunc(mousePressedOrReleased);
    glutPassiveMotionFunc(mouseMoved);

    glutMainLoop();
    return 0;
}

void reshape(int width, int height)
{
    glDisable(GL_DEPTH_TEST);
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, width-1, height-1, 0, 0, 1);
    glMatrixMode(GL_MODELVIEW);
}

void display()
{
    if (convexity) {
        glClearColor(0.0f, 1.0f, 0.0f, 1.0f);
    } else {
        glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    }
    glClear(GL_COLOR_BUFFER_BIT);
    glLoadIdentity();
    renderScene();
    glutSwapBuffers();
}

void renderScene()
{
    glColor3f(0.0f, 0.0f, 0.0f);
    if (!filling) {
        glBegin(GL_LINE_LOOP);
        for (PolygonElement element : polygon) {
            glVertex2i(element.vertex.x, element.vertex.y);
        }
        glEnd();
    } else {
        fillPolygon(polygon);
    }
}

void keyPressed(unsigned char key, int x, int y)
{
    if (key == 'k') {
        if (drawing_state) {
            if (convexity) {
                convexity = false;
                glutPostRedisplay();
            } else if (isConvex(polygon)) {
                convexity = true;
                glutPostRedisplay();
            } else {
                std::cout << "Polygon not convex. Convexity checking NOT enabled." << std::endl;
            }
        }
    } else if (key == 'p') {
        if (drawing_state) {
            filling = !filling;
            glutPostRedisplay();
        }
    } else if (key == 'n') {
        drawing_state = !drawing_state;
        if (drawing_state) {
            polygon.clear();
            polygon.push_back({{x, y}, {0, 0, 0}, false});
            filling = false;
            convexity = false;
        } else {
            polygon.pop_back();
            calculatePolygonCoefficients(polygon);
        }
        glutPostRedisplay();
    }
}

void mousePressedOrReleased(int button, int state, int x, int y)
{
    if (button == GLUT_LEFT_BUTTON && state == GLUT_UP) {
        if (drawing_state) {
            if (convexity) {
                if (isConvex(polygon)) {
                    polygon.push_back({{x, y}, {0, 0, 0}, false});
                    calculatePolygonCoefficients(polygon);
                    glutPostRedisplay();
                } else {
                    std::cout << "Polygon not convex. Skipping." << std::endl;
                }
            } else {
                polygon.push_back({{x, y}, {0, 0, 0}, false});
                calculatePolygonCoefficients(polygon);
                glutPostRedisplay();
            }
        } else {
            int status = polygonPointStatus(polygon, x, y);
            std::cout << "Point (" << x << ", " << y << ')' << std::endl;
            std::cout << "Status: ";
            if (status == POINT_OUTSIDE) {
                std::cout << "OUTSIDE";
            } else if (status == POINT_INSIDE) {
                std::cout << "INSIDE";
            } else {
                std::cout << "ON";
            }
            std::cout << std::endl;
        }
    }
}

void mouseMoved(int x, int y)
{
    if (drawing_state) {
        if (!polygon.empty()) {
            Point& last = polygon.back().vertex;
            last.x = x;
            last.y = y;
            calculatePolygonCoefficients(polygon);
            glutPostRedisplay();
        }
    }
}
