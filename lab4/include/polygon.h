#ifndef POLYGON_H
#define POLYGON_H

struct Point {
    int x, y;
};

struct Edge {
    int a, b, c;
};

struct PolygonElement {
    Point vertex;
    Edge edge;
    bool left;
};

void calculatePolygonCoefficients(std::vector<PolygonElement>& elements)
{
    int n = elements.size();
    if (n < 2) return;
    int i0 = n-1;
    for (int i = 0; i < n; i ++) {
        elements[i0].edge.a = elements[i0].vertex.y - elements[i].vertex.y;
        elements[i0].edge.b = -(elements[i0].vertex.x - elements[i].vertex.x);
        elements[i0].edge.c = elements[i0].vertex.x*elements[i].vertex.y
                            - elements[i0].vertex.y*elements[i].vertex.x;
        elements[i0].left = elements[i0].vertex.y < elements[i].vertex.y;
        i0 = i;
    }
}

void checkPolygonConvexity(const std::vector<PolygonElement>& elements, bool& convex, bool& clockwise)
{
    int above = 0, below = 0, on = 0;
    int n = elements.size();
    int i0 = n-2;

    for (int i = 0; i < n; i ++) {
        if (i0 >= n) i0 = 0;
        int r = elements[i0].edge.a*elements[i].vertex.x
            + elements[i0].edge.b*elements[i].vertex.y
            + elements[i0].edge.c;
        if (r == 0) on ++;
        else if (r > 0) above ++;
        else below ++;
    }

    convex = false;
    clockwise = false;
    if (below == 0) {
        convex = true;
    } else if (above == 0) {
        convex = true;
        clockwise = true;
    }
}

void fillPolygon(const std::vector<PolygonElement>& elememts)
{
    int xmin, xmax, ymin, ymax;
    xmin = xmax = elememts[0].vertex.x;
    ymin = ymax = elememts[0].vertex.y;

    int n = elememts.size();
    if (n < 3) return;
    bool convex;
    bool clockwise;
    checkPolygonConvexity(elememts, convex, clockwise);

    for (int i = 1; i < n; i ++) {
        if (xmin > elememts[i].vertex.x) xmin = elememts[i].vertex.x;
        if (xmax < elememts[i].vertex.x) xmax = elememts[i].vertex.x;
        if (ymin > elememts[i].vertex.y) ymin = elememts[i].vertex.y;
        if (ymax < elememts[i].vertex.y) ymax = elememts[i].vertex.y;
    }

    for (int y = ymin; y <= ymax; y ++) {
        double L = xmin;
        double D = xmax;
        int i0 = n-1;
        for (int i = 0; i < n; i0 = i++) {
            if (elememts[i0].edge.a == 0) {
                if (elememts[i0].vertex.y == y) {
                    if (elememts[i0].vertex.x < elememts[i].vertex.x) {
                        L = elememts[i0].vertex.x;
                        D = elememts[i].vertex.x;
                    } else {
                        L = elememts[i].vertex.x;
                        D = elememts[i0].vertex.x;
                    }
                    break;
                }
            } else {
                double x = (-elememts[i0].edge.b*y - elememts[i0].edge.c)
                    / (double) elememts[i0].edge.a;
                if (elememts[i0].left ^ !clockwise) {
                    if (L < x) L = x;
                } else {
                    if (D > x) D = x;
                }
            }
        }

        glBegin(GL_LINES);
        glVertex2i(std::round(L), y);
        glVertex2i(std::round(D), y);
        glEnd();
    }
}

bool isConvex(const std::vector<PolygonElement>& elements) {
    bool convex;
    bool clockwise;
    checkPolygonConvexity(elements, convex, clockwise);
    return convex;
}

bool isClockwise(const std::vector<PolygonElement>& elements) {
    bool convex;
    bool clockwise;
    checkPolygonConvexity(elements, convex, clockwise);
    return clockwise;
}

#define POINT_INSIDE 1
#define POINT_OUTSIDE -1
#define POINT_ON 0

int polygonPointStatus(const std::vector<PolygonElement>& elements, int x, int y)
{
    bool clockwise = isClockwise(elements);
    for (const PolygonElement& element : elements) {
        int dot_product = x*element.edge.a + y*element.edge.b + element.edge.c;
        if (clockwise) {
            if (dot_product > 0) return POINT_OUTSIDE;
        } else {
            if (dot_product < 0) return POINT_OUTSIDE;
        }
        if (dot_product == 0) return POINT_ON;
    }
    return POINT_INSIDE;
}

#endif // POLYGON_H
