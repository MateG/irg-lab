#include "Sphere.h"

#include <cmath>

Sphere::Sphere(std::vector<double> amb_rgb,
        std::vector<double> dif_rgb, std::vector<double> ref_rgb,
        double n, double k_ref,
        IVector &center, double radius)
        : SceneObject(amb_rgb, dif_rgb, ref_rgb, n, k_ref,
                amb_rgb, dif_rgb, ref_rgb, n, k_ref),
                center_(center), radius_(radius) {
}

Sphere::~Sphere() {
    delete &center_;
}

void Sphere::UpdateIntersection(Intersection& intersection, IVector &start, IVector &d) {
    std::unique_ptr<IVector> oc = start.n_Sub(center_);
    double loc = d.ScalarProduct(*oc);
    double oc_norm = oc->Norm();
    double radicand = loc*loc - oc_norm*oc_norm + radius_*radius_;
    if (radicand < 0.0) return;

    double root = std::sqrt(radicand);
    double lambda = -loc - root;
    if (lambda < 0.0) {
        lambda = -loc + root;
        if (lambda < 0.0) return;
    }

    std::shared_ptr<IVector> point = start.n_Add(
            *(d.n_ScalarMultiply(lambda)));
    if (intersection.object_ == nullptr || lambda < intersection.lambda_) {
        intersection = {this, lambda, true, point};
    }
}

std::unique_ptr<IVector> Sphere::GetNormalInPoint(IVector &point) {
    std::unique_ptr<IVector> normal = point.n_Sub(center_);
    normal->Normalize();
    return normal;
}
