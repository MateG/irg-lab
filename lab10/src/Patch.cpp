#include "Patch.h"

#include <utility>
#include "IRG/Matrix.h"
#include "IRG/Vector.h"

Patch::Patch(std::vector<double> f_amb_rgb, std::vector<double> f_dif_rgb,
        std::vector<double> f_ref_rgb, double f_n, double f_k_ref,
        std::vector<double> b_amb_rgb, std::vector<double> b_dif_rgb,
        std::vector<double> b_ref_rgb, double b_n, double b_k_ref,
        IVector &center, IVector &v1, IVector &v2, double w, double h)
: SceneObject(std::move(f_amb_rgb), std::move(f_dif_rgb),
        std::move(f_ref_rgb), f_n, f_k_ref, std::move(b_amb_rgb),
        std::move(b_dif_rgb), std::move(b_ref_rgb), b_n, b_k_ref),
        center_(center), v1_(v1), v2_(v2), w_(w), h_(h) {
    normal_ = v1.n_VectorProduct(v2);
    normal_->Normalize();
}

Patch::~Patch() {
    delete &center_;
    delete &v1_;
    delete &v2_;
}

void Patch::UpdateIntersection(Intersection& intersection, IVector& start, IVector& d) {
    Matrix coefficient_matrix = {
            {v1_.Get(0), v2_.Get(0), -d.Get(0)},
            {v1_.Get(1), v2_.Get(1), -d.Get(1)},
            {v1_.Get(2), v2_.Get(2), -d.Get(2)}
    };
    double denominator = coefficient_matrix.Determinant();

    double sc_x = start.Get(0) - center_.Get(0);
    double sc_y = start.Get(1) - center_.Get(1);
    double sc_z = start.Get(2) - center_.Get(2);

    Matrix x_matrix = {
            {sc_x, v2_.Get(0), -d.Get(0)},
            {sc_y, v2_.Get(1), -d.Get(1)},
            {sc_z, v2_.Get(2), -d.Get(2)}
    };
    double x_det = x_matrix.Determinant();
    Matrix y_matrix = {
            {v1_.Get(0), sc_x, -d.Get(0)},
            {v1_.Get(1), sc_y, -d.Get(1)},
            {v1_.Get(2), sc_z, -d.Get(2)}
    };
    double y_det = y_matrix.Determinant();
    Matrix z_matrix = {
            {v1_.Get(0), v2_.Get(0), sc_x},
            {v1_.Get(1), v2_.Get(1), sc_y},
            {v1_.Get(2), v2_.Get(2), sc_z}
    };
    double z_det = z_matrix.Determinant();

    double z = z_det/denominator;
    if (z < 0.0) return;
    if (intersection.point_ != nullptr && z > intersection.lambda_) return;
    double x = x_det/denominator;
    if (x < -w_/2.0 || x > w_/2.0) return;
    double y = y_det/denominator;
    if (y < -h_/2.0 || y > h_/2.0) return;

    double scalar = d.ScalarProduct(*normal_);
    std::shared_ptr<IVector> point = start.n_Add(*(d.n_ScalarMultiply(z)));
    intersection = {this, z, scalar < 0, point};
}

std::unique_ptr<IVector> Patch::GetNormalInPoint(IVector &point) {
    return normal_->Copy();
}
