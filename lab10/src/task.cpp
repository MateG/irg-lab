#include <iostream>
#include <limits>
#include <cmath>
#include <GL/glut.h>
#include "IRG/Vector.h"
#include "RTScene.h"

void reshape(int width, int height);
void display();
void renderScene();

void keyPressed(unsigned char key, int x, int y);

typedef struct {
    double r, g, b;
} Color;

void trace(IVector& start, IVector& direction, Color& color, int counter);
void updateColor(IVector& start, IVector& direction,
        Intersection& intersection, Color& color, int counter);
double diffuseAndReflective(double intensity, double f_d, double f_r,
        double f_n, double ln_dot, double rv_dot);

int current_width = 400;
int current_height = 300;

std::unique_ptr<RTScene> scene;
std::unique_ptr<IVector> look;

const int recursive_count = 10;

int main(int argc, char** argv) {
    if (argc != 2) {
        std::cerr << "Expected 1 argument - path to the scene definition file." << std::endl;
        return 1;
    }

    scene = RTScene::LoadScene(argv[1]);

    glutInit(&argc, argv);
    glutInitWindowSize(current_width, current_height);
    glutInitWindowPosition(400, 100);
    glutCreateWindow("Task");

    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutKeyboardFunc(keyPressed);

    glutMainLoop();
    return 0;
}

void reshape(int width, int height)
{
    current_width = width;
    current_height = height;

    glDisable(GL_DEPTH_TEST);
    glViewport(0, 0, (GLsizei) width, (GLsizei) height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, width, 0, height, 0, 1);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void display()
{
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    renderScene();
    glutSwapBuffers();
}

void renderScene() {
    std::unique_ptr<IVector> view_normalized = scene->view_.n_Normalize();
    look = scene->eye_.n_Add(
            *(view_normalized->n_ScalarMultiply(scene->h_)));

    std::cout << "Rendering..." << std::endl;
    glBegin(GL_POINTS);
    for (int y = 0; y < current_height; y ++) {
        for (int x = 0; x < current_width; x ++) {
            std::unique_ptr<IVector> offset_x = scene->x_axis_->n_ScalarMultiply(
                    -scene->l_ + (scene->l_ + scene->r_)*x/current_width);
            std::unique_ptr<IVector> offset_y = scene->y_axis_->n_ScalarMultiply(
                    -scene->b_ + (scene->t_ + scene->b_)*y/current_height);

            std::unique_ptr<IVector> start = look->n_Add(*offset_x);
            start->Add(*offset_y);
            std::unique_ptr<IVector> direction = start->n_Sub(scene->eye_);
            direction->Normalize();

            Color color = {0.0, 0.0, 0.0};
            trace(*start, *direction, color, recursive_count);
            glColor3d(color.r, color.g, color.b);
            glVertex2i(x, y);
        }
    }
    glEnd();
    std::cout << "Done." << std::endl;
}

void trace(IVector& start, IVector& direction, Color& color, int counter) {
    if (counter == 0) return;

    Intersection intersection = {nullptr, 0.0, false, nullptr};
    for (SceneObject* object : scene->objects_) {
        object->UpdateIntersection(intersection, start, direction);
    }

    if (intersection.object_ != nullptr) {
        updateColor(start, direction, intersection, color, counter);
    }
}

#define EPSILON 1E-4
void updateColor(IVector& start, IVector& direction,
        Intersection& intersection, Color& color, int counter) {
    SceneObject* object = intersection.object_;
    color = {scene->ga_intensity_[0], scene->ga_intensity_[1], scene->ga_intensity_[2]};
    std::vector<double> dif;
    std::vector<double> ref;
    double k_ref;
    if (intersection.front_) {
        color.r *= object->f_amb_rgb[0];
        color.g *= object->f_amb_rgb[1];
        color.b *= object->f_amb_rgb[2];
        dif = object->f_dif_rgb;
        ref = object->f_ref_rgb;
        k_ref = object->f_k_ref;
    } else {
        color.r *= object->b_amb_rgb[0];
        color.g *= object->b_amb_rgb[1];
        color.b *= object->b_amb_rgb[2];
        dif = object->b_dif_rgb;
        ref = object->b_ref_rgb;
        k_ref = object->b_k_ref;
    }

    std::unique_ptr<IVector> normal = object->GetNormalInPoint(
            *intersection.point_);
    if (!intersection.front_) normal->ScalarMultiply(-1.0);

    for (const Light& light : scene->sources_) {
        std::unique_ptr<IVector> towards_light = light.position_.n_Sub(
                *intersection.point_);
        towards_light->Normalize();
        std::unique_ptr<IVector> shadow_start = intersection.point_->n_Add(
                *(towards_light->n_ScalarMultiply(EPSILON)));

        Intersection shadow_intersection = {nullptr, 0.0, false, nullptr};
        for (SceneObject* other_object : scene->objects_) {
            other_object->UpdateIntersection(
                    shadow_intersection, *shadow_start, *towards_light);
        }

        if (shadow_intersection.object_ != nullptr) {
            double light_distance = light.position_.n_Sub(
                    *intersection.point_)->Norm();
            double object_distance = shadow_intersection.point_->n_Sub(
                    *intersection.point_)->Norm();

            if (object_distance < light_distance) continue;
        }

        double ln_dot = std::max(towards_light->ScalarProduct(*normal), 0.0);
        std::unique_ptr<IVector> r = normal->n_ScalarMultiply(
                2.0*normal->ScalarProduct(*towards_light))->n_Sub(*towards_light);
        std::unique_ptr<IVector> v = scene->eye_.n_Sub(*intersection.point_);
        v->Normalize();
        double rv_dot = std::max(r->ScalarProduct(*v), 0.0);
        double f_n = object->f_n;

        color.r += diffuseAndReflective(light.rgb_[0],
                dif[0], ref[0], f_n, ln_dot, rv_dot);
        color.g += diffuseAndReflective(light.rgb_[1],
                dif[1], ref[1], f_n, ln_dot, rv_dot);
        color.b += diffuseAndReflective(light.rgb_[2],
                dif[2], ref[2], f_n, ln_dot, rv_dot);
    }

    Color reflected_color = {0.0, 0.0, 0.0};
    std::unique_ptr<IVector> reflected_direction = direction.n_Sub(
            *(normal->n_ScalarMultiply(2.0*direction.ScalarProduct(*normal))));
    std::unique_ptr<IVector> reflected_start = intersection.point_->n_Add(
            *(reflected_direction->n_ScalarMultiply(EPSILON)));
    trace(*reflected_start, *reflected_direction, reflected_color, counter-1);

    color.r += k_ref*reflected_color.r;
    color.g += k_ref*reflected_color.g;
    color.b += k_ref*reflected_color.b;
}

double diffuseAndReflective(double intensity, double d, double r,
        double f_n, double ln_dot, double rv_dot) {
    return intensity * (d*ln_dot + r*std::pow(rv_dot, f_n));
}
#undef EPSILON

void keyPressed(unsigned char key, int x, int y) {
    if (key == 'a') {
        scene->eye_.Set(0, scene->eye_.Get(0)-0.1);
        glutPostRedisplay();
    } else if (key == 'd') {
        scene->eye_.Set(0, scene->eye_.Get(0)+0.1);
        glutPostRedisplay();
    } else if (key == 's') {
        scene->eye_.Set(1, scene->eye_.Get(1)-0.1);
        glutPostRedisplay();
    } else if (key == 'w') {
        scene->eye_.Set(1, scene->eye_.Get(1)+0.1);
        glutPostRedisplay();
    }
}