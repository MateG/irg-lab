#include <SceneObject.h>

SceneObject::SceneObject(std::vector<double> f_amb_rgb, std::vector<double> f_dif_rgb,
        std::vector<double> f_ref_rgb, double f_n, double f_k_ref,
        std::vector<double> b_amb_rgb, std::vector<double> b_dif_rgb,
        std::vector<double> b_ref_rgb, double b_n, double b_k_ref)
        : f_amb_rgb(std::move(f_amb_rgb)), f_dif_rgb(std::move(f_dif_rgb)),
        f_ref_rgb(std::move(f_ref_rgb)), f_n(f_n), f_k_ref(f_k_ref),
        b_amb_rgb(std::move(b_amb_rgb)), b_dif_rgb(std::move(b_dif_rgb)),
        b_ref_rgb(std::move(b_ref_rgb)), b_n(b_n), b_k_ref(b_k_ref) {
}

