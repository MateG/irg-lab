#include "RTScene.h"

#include <fstream>
#include <sstream>
#include <iostream>
#include <cmath>
#include "IRG/Vector.h"
#include "Sphere.h"
#include "Patch.h"

RTScene::RTScene(IVector& eye, IVector& view, IVector& view_up,
        double h, double x_angle, double y_angle, const std::vector<double>& ga_intensity,
        const std::vector<Light>& sources, const std::vector<SceneObject*>& objects)
        : eye_(eye), view_(view), view_up_(view_up), h_(h), x_angle_(x_angle), y_angle_(y_angle),
        ga_intensity_(ga_intensity),sources_(sources), objects_(objects) {
    ComputeKS();
}


RTScene::~RTScene() {
    delete &eye_;
    delete &view_;
    delete &view_up_;
    for (auto object : objects_) {
        delete object;
    }
}

#define RAD(x) ((x)*M_PI/180.0)
void RTScene::ComputeKS() {
    x_axis_ = view_.n_VectorProduct(view_up_);
    x_axis_->Normalize();
    y_axis_ = x_axis_->n_VectorProduct(view_);
    y_axis_->Normalize();
    l_ = std::tan(RAD(x_angle_/2.0)) * h_;
    r_ = l_;
    t_ = std::tan(RAD(y_angle_/2.0)) * h_;
    b_ = t_;
}
#undef RAD

void warn() {
    std::cerr << "[WARNING] Encountered invalid data while parsing" << std::endl;
}

IVector* parse3DVector(std::istringstream& iss) {
    double a, b, c;
    if (!(iss >> a)) return nullptr;
    if (!(iss >> b)) return nullptr;
    if (!(iss >> c)) return nullptr;
    return new Vector({a, b, c});
}

Light parseLight(std::istringstream& iss) {
    double x, y, z, r, g, b;
    if (!(iss >> x)) warn();
    if (!(iss >> y)) warn();
    if (!(iss >> z)) warn();
    if (!(iss >> r)) warn();
    if (!(iss >> g)) warn();
    if (!(iss >> b)) warn();
    return {*(new Vector({x, y, z})), {r, g, b}};
}

std::vector<double> parseVector(std::istringstream& iss, size_t size) {
    double value;
    std::vector<double> vector(size);
    for (int i = 0; i < size; i ++) {
        if (!(iss >> value)) return vector;
        vector[i] = value;
    }
    return vector;
}

Sphere* parseSphere(std::istringstream& iss) {
    std::vector<double> center_values = parseVector(iss, 3);
    if (center_values.size() < 3) return nullptr;
    IVector* center = new Vector(center_values);
    double radius;
    if (!(iss >> radius)) return nullptr;

    std::vector<double> amb_rgb = parseVector(iss, 3);
    if (amb_rgb.size() < 3) return nullptr;
    std::vector<double> dif_rgb = parseVector(iss, 3);
    if (dif_rgb.size() < 3) return nullptr;
    std::vector<double> ref_rgb = parseVector(iss, 3);
    if (ref_rgb.size() < 3) return nullptr;

    double n;
    if (!(iss >> n)) return nullptr;
    double k_ref;
    if (!(iss >> k_ref)) return nullptr;

    return new Sphere(amb_rgb, dif_rgb, ref_rgb, n, k_ref, *center, radius);
}

Patch* parsePatch(std::istringstream& iss) {
    std::vector<double> center_values = parseVector(iss, 3);
    if (center_values.size() < 3) return nullptr;
    IVector* center = new Vector(center_values);

    std::vector<double> v1_values = parseVector(iss, 3);
    if (v1_values.size() < 3) return nullptr;
    IVector* v1 = new Vector(v1_values);

    std::vector<double> v2_values = parseVector(iss, 3);
    if (v2_values.size() < 3) return nullptr;
    IVector* v2 = new Vector(v2_values);

    double width;
    if (!(iss >> width)) return nullptr;
    double height;
    if (!(iss >> height)) return nullptr;

    std::vector<double> f_amb = parseVector(iss, 3);
    if (f_amb.size() < 3) return nullptr;
    std::vector<double> f_dif = parseVector(iss, 3);
    if (f_dif.size() < 3) return nullptr;
    std::vector<double> f_ref = parseVector(iss, 3);
    if (f_ref.size() < 3) return nullptr;

    double f_n;
    if (!(iss >> f_n)) return nullptr;
    double f_k_ref;
    if (!(iss >> f_k_ref)) return nullptr;

    std::vector<double> b_amb = parseVector(iss, 3);
    if (b_amb.size() < 3) return nullptr;
    std::vector<double> b_dif = parseVector(iss, 3);
    if (b_dif.size() < 3) return nullptr;
    std::vector<double> b_ref = parseVector(iss, 3);
    if (b_ref.size() < 3) return nullptr;

    double b_n;
    if (!(iss >> b_n)) return nullptr;
    double b_k_ref;
    if (!(iss >> b_k_ref)) return nullptr;

    return new Patch(f_amb, f_dif, f_ref, f_n, f_k_ref, b_amb, b_dif, b_ref, b_n, b_k_ref, *center, *v1, *v2, width, height);
}

SceneObject* parseSceneObject(std::istringstream& iss) {
    char type;
    if (!(iss >> type)) return nullptr;
    switch (type) {
        case 's': return parseSphere(iss);
        case 'p': return parsePatch(iss);
        default: return nullptr;
    }
}

std::unique_ptr<RTScene> RTScene::LoadScene(const std::string& path) {
    std::ifstream file_stream(path);
    if (!file_stream.good()) throw std::runtime_error("File not found.");

    IVector* eye = new Vector({0, 0, 0});
    IVector* view = new Vector({1, 0, 0});
    IVector* view_up = new Vector({0, 0, 1});
    double h = 1;
    double x_angle = 0;
    double y_angle = 0;
    std::vector<double> ga_intensity = {0, 0, 0};
    std::vector<Light> sources;
    std::vector<SceneObject*> objects;

    std::string line;
    std::string type;
    while (std::getline(file_stream, line)) {
        std::istringstream iss(line);
        if (!(iss >> type)) continue;
        if (type == "#") {
            // Skip
        } else if (type == "e") {
            eye = parse3DVector(iss);
            if (eye == nullptr) warn();
        } else if (type == "v") {
            view = parse3DVector(iss);
            if (view == nullptr) warn();
        } else if (type == "vu") {
            view_up = parse3DVector(iss);
            if (view_up == nullptr) warn();
        } else if (type == "h") {
            if (!(iss >> h)) warn();
        } else if (type == "xa") {
            if (!(iss >> x_angle)) warn();
        } else if (type == "ya") {
            if (!(iss >> y_angle)) warn();
        } else if (type == "ga") {
            double value;
            ga_intensity.clear();
            while (iss >> value) ga_intensity.push_back(value);
            if (ga_intensity.size() != 3) warn();
        } else if (type == "i") {
            sources.push_back(parseLight(iss));
        } else if (type == "o") {
            SceneObject* object = parseSceneObject(iss);
            if (object == nullptr) warn();
            objects.push_back(object);
        }
    }
    if (eye == nullptr || view == nullptr || view_up == nullptr) {
        throw std::runtime_error("Error while parsing eye/view/view_up.");
    }
    return std::make_unique<RTScene>(
            *eye, *view, *view_up, h, x_angle, y_angle, ga_intensity, sources, objects);
}
