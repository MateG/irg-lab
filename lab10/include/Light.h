#ifndef LIGHT_H
#define LIGHT_H

#include <vector>
#include "IRG/IVector.h"

struct Light {
    IVector& position_;
    std::vector<double> rgb_;
};

#endif //LIGHT_H
