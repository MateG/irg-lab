#ifndef SPHERE_H
#define SPHERE_H

#include "SceneObject.h"

class Sphere : public SceneObject {
protected:
    IVector& center_;
    double radius_;
public:
    Sphere(std::vector<double> amb_rgb,
           std::vector<double> dif_rgb,
           std::vector<double> ref_rgb,
           double n, double k_ref,
           IVector &center, double radius);
    ~Sphere() override;

    void UpdateIntersection(Intersection& intersection, IVector &start, IVector &d) override;
    std::unique_ptr<IVector> GetNormalInPoint(IVector &point) override;
};

#endif //SPHERE_H
