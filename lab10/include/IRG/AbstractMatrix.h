#ifndef ABSTRACTMATRIX_H
#define ABSTRACTMATRIX_H

#include "IMatrix.h"

class AbstractMatrix : public IMatrix
{
private:
    std::ostream& print(std::ostream& os) const override;
public:
    virtual ~AbstractMatrix();

    // TODO dokumentirati da ako je live_view, pogled ne postaje vlasnikom ove matrice
    std::unique_ptr<IMatrix> n_Transpose(bool live_view) override;
    IMatrix& Add(const IMatrix& other) override;
    std::unique_ptr<IMatrix> n_Add(const IMatrix& other) const override;
    IMatrix& Sub(const IMatrix& other) override;
    std::unique_ptr<IMatrix> n_Sub(const IMatrix& other) const override;
    std::unique_ptr<IMatrix> n_Multiply(const IMatrix& other) const override;
    double Determinant() override;
    std::unique_ptr<IMatrix> SubMatrix(int row, int col, bool live_view) override;
    std::unique_ptr<IMatrix> n_Invert() override;
    std::vector<std::vector<double>> ToArray() const override;
    std::unique_ptr<IVector> ToVector(bool live_view) override;
};

#endif // ABSTRACTMATRIX_H
