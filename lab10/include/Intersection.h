#ifndef INTERSECTION_H
#define INTERSECTION_H

class SceneObject;

struct Intersection {
    SceneObject* object_;
    double lambda_;
    bool front_;
    std::shared_ptr<IVector> point_;
};

#endif //INTERSECTION_H
