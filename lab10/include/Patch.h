#ifndef PATCH_H
#define PATCH_H

#include "SceneObject.h"
#include "IRG/IVector.h"

class Patch : public SceneObject {
protected:
    IVector& center_;
    IVector& v1_;
    IVector& v2_;
    std::unique_ptr<IVector> normal_;
    double w_;
    double h_;
public:
    Patch(std::vector<double> f_amb_rgb,
          std::vector<double> f_dif_rgb,
          std::vector<double> f_ref_rgb,
          double f_n, double f_k_ref,
          std::vector<double> b_amb_rgb,
          std::vector<double> b_dif_rgb,
          std::vector<double> b_ref_rgb,
          double b_n, double b_k_ref,
          IVector& center, IVector& v1,
          IVector& v2, double w, double h);
    ~Patch() override;

    void UpdateIntersection(Intersection& intersection, IVector& start, IVector& d) override;
    std::unique_ptr<IVector> GetNormalInPoint(IVector& point) override;
};

#endif //PATCH_H
