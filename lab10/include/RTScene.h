#ifndef RTSCENE_H
#define RTSCENE_H

#include "IRG/IVector.h"
#include "Light.h"
#include "SceneObject.h"

class RTScene {
public:
    IVector& eye_;
    IVector& view_;
    IVector& view_up_;
    double h_;
    double x_angle_;
    double y_angle_;
    std::vector<double> ga_intensity_;
    std::vector<Light> sources_;
    std::vector<SceneObject*> objects_;

    std::unique_ptr<IVector> x_axis_;
    std::unique_ptr<IVector> y_axis_;
    double l_;
    double r_;
    double b_;
    double t_;
private:
    void ComputeKS();
public:
    RTScene(IVector& eye_, IVector& view_, IVector& view_up_,
    double h_, double x_angle_, double y_angle_, const std::vector<double>& ga_intensity_,
    const std::vector<Light>& sources_, const std::vector<SceneObject*>& objects_);
    virtual ~RTScene();
    static std::unique_ptr<RTScene> LoadScene(const std::string& path);
};

#endif //RTSCENE_H
