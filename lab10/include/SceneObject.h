#ifndef SCENEOBJECT_H
#define SCENEOBJECT_H

#include <vector>
#include "IRG/IVector.h"
#include "Intersection.h"

class SceneObject {
public:
    std::vector<double> f_amb_rgb;
    std::vector<double> f_dif_rgb;
    std::vector<double> f_ref_rgb;
    double f_n;
    double f_k_ref;

    std::vector<double> b_amb_rgb;
    std::vector<double> b_dif_rgb;
    std::vector<double> b_ref_rgb;
    double b_n;
    double b_k_ref;
public:
    SceneObject(std::vector<double> f_amb_rgb,
            std::vector<double> f_dif_rgb,
            std::vector<double> f_ref_rgb,
            double f_n, double f_k_ref,
            std::vector<double> b_amb_rgb,
            std::vector<double> b_dif_rgb,
            std::vector<double> b_ref_rgb,
            double b_n, double b_k_ref);
    virtual ~SceneObject() = default;
    virtual void UpdateIntersection(Intersection& intersection, IVector& start, IVector& d) = 0;
    virtual std::unique_ptr<IVector> GetNormalInPoint(IVector& point) = 0;
};

#endif //SCENEOBJECT_H
