#include "MatrixTransposeView.h"

#include "IrgException.h"

MatrixTransposeView::MatrixTransposeView(IMatrix& original)
: original_(original)
{
}

MatrixTransposeView::~MatrixTransposeView()
{
}

int MatrixTransposeView::GetRowsCount() const
{
    return original_.GetColsCount();
}

int MatrixTransposeView::GetColsCount() const
{
    return original_.GetRowsCount();
}

double MatrixTransposeView::Get(int row, int col) const
{
    return original_.Get(col, row);
}

IMatrix& MatrixTransposeView::Set(int row, int col, double value)
{
    original_.Set(col, row, value);
    return *this;
}

std::unique_ptr<IMatrix> MatrixTransposeView::Copy() const
{
    return std::make_unique<MatrixTransposeView>(original_);
}

std::unique_ptr<IMatrix> MatrixTransposeView::NewInstance(int rows, int cols) const
{
    throw IrgException("MatrixTransposeView::NewInstance is not implemented!");
}
