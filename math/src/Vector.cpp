#include "Vector.h"

#include <sstream>
#include <iterator>
#include "IrgException.h"

Vector::Vector(const Vector& vector)
: read_only_(vector.read_only_)
{
    elements_ = std::make_unique<std::vector<double>>(*vector.elements_);
}

Vector::Vector(Vector&& vector)
: read_only_(vector.read_only_)
{
    elements_ = std::move(vector.elements_);
}

Vector::Vector(std::vector<double>& elements)
: Vector(false, false, elements)
{
}

Vector::Vector(bool immutable, bool store_reference, std::vector<double>& elements)
{
    elements_ = store_reference
            ? std::unique_ptr<std::vector<double>>(&elements)
            : std::make_unique<std::vector<double>>(elements);
    read_only_ = immutable;
}

Vector::Vector(const std::initializer_list<double>& list)
{
    elements_ = std::make_unique<std::vector<double>>(list);
}

Vector::~Vector()
{
}

double Vector::Get(int index) const
{
    return (*elements_)[index];
}

IVector& Vector::Set(int index, double value)
{
    if (read_only_) {
        throw IrgException("Vector is read only!");
    }

    (*elements_)[index] = value;
    return *this;
}

int Vector::GetDimension() const
{
    return elements_->size();
}

std::unique_ptr<IVector> Vector::Copy() const
{
    return std::make_unique<Vector>(*elements_);
}

std::unique_ptr<IVector> Vector::NewInstance(int dimension) const
{
    std::vector<double> elements(dimension);
    return std::make_unique<Vector>(elements);
}

std::unique_ptr<Vector> Vector::ParseSimple(const std::string& text)
{
    std::istringstream stream(text);
    std::vector<double> elements {
        std::istream_iterator<double>(stream),
        std::istream_iterator<double>()
    };
    return std::make_unique<Vector>(elements);
}
