#include "IrgException.h"

IrgException::IrgException(const char* message)
: std::runtime_error(message)
{
}
