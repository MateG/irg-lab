#include "MatrixVectorView.h"

#include "IVector.h"
#include "IrgException.h"

MatrixVectorView::MatrixVectorView(IVector& original, bool as_row_matrix)
: original_(original), as_row_matrix_(as_row_matrix)
{
}

MatrixVectorView::~MatrixVectorView()
{
}

int MatrixVectorView::GetRowsCount() const
{
    return as_row_matrix_ ? 1 : original_.GetDimension();
}

int MatrixVectorView::GetColsCount() const
{
    return as_row_matrix_ ? original_.GetDimension() : 1;
}

// TODO dokumentirati da se uvijek ignorira jedan od ovih
double MatrixVectorView::Get(int row, int col) const
{
    return as_row_matrix_ ? original_.Get(col) : original_.Get(row);
}

// TODO dokumentirati da se uvijek ignorira jedan od ovih
IMatrix& MatrixVectorView::Set(int row, int col, double value)
{
    if (as_row_matrix_) {
        original_.Set(col, value);
    } else {
        original_.Set(row, value);
    }
    return *this;
}

std::unique_ptr<IMatrix> MatrixVectorView::Copy() const
{
    return std::make_unique<MatrixVectorView>(original_, as_row_matrix_);
}

std::unique_ptr<IMatrix> MatrixVectorView::NewInstance(int rows, int cols) const
{
    throw IrgException("MatrixVectorView::NewInstance is not implemented!");
}
