#include "IVector.h"

IVector::~IVector()
{
}

double IVector::operator[](int index) const
{
    return Get(index);
}

std::ostream& operator<<(std::ostream& os, const IVector& vector)
{
    return vector.print(os);
}
