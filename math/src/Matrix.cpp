#include "Matrix.h"

#include <sstream>
#include <iterator>
#include "IrgException.h"

#define POSITIVE(x, name) \
    (x > 0 ? x : throw IrgException(name" count must be positive!"))

Matrix::Matrix(int rows, int cols)
: rows_(POSITIVE(rows, "Row")), cols_(POSITIVE(cols, "Column"))
{
    elements_ = std::make_unique<std::vector<std::vector<double>>>(rows, std::vector<double>(cols));
}

Matrix::Matrix(int rows, int cols, std::vector<std::vector<double>>& elements, bool store_reference)
: rows_(POSITIVE(rows, "Row")), cols_(POSITIVE(cols, "Column"))
{
    if ((int) elements.size() != rows) {
        throw IrgException("Conflicting row count values!");
    }

    for (const auto& row : elements) {
        if ((int) row.size() != cols) {
            throw IrgException("Conflicting column count values!");
        }
    }

    elements_ = store_reference
            ? std::unique_ptr<std::vector<std::vector<double>>>(&elements)
            : std::make_unique<std::vector<std::vector<double>>>(elements);
}

Matrix::Matrix(const std::initializer_list<std::initializer_list<double>>& list)
{
    rows_ = list.size();
    auto it = list.begin();
    cols_ = it->size();
    it ++;
    while (it != list.end()) {
        if ((int) it->size() != cols_) {
            throw IrgException("Conflicting column count values!");
        }
        it ++;
    }

    elements_ = std::make_unique<std::vector<std::vector<double>>>(rows_, std::vector<double>(cols_));
    int row = 0;
    for (const auto& row_values : list) {
        int col = 0;
        for (double value : row_values) {
            (*elements_)[row][col] = value;
            col ++;
        }
        row ++;
    }
}

Matrix::~Matrix()
{
}

int Matrix::GetRowsCount() const
{
    return rows_;
}

int Matrix::GetColsCount() const
{
    return cols_;
}

double Matrix::Get(int row, int col) const
{
    return (*elements_)[row][col];
}

IMatrix& Matrix::Set(int row, int col, double value)
{
    (*elements_)[row][col] = value;
    return *this;
}

std::unique_ptr<IMatrix> Matrix::Copy() const
{
    return std::make_unique<Matrix>(rows_, cols_, *elements_, false);
}

std::unique_ptr<IMatrix> Matrix::NewInstance(int rows, int cols) const
{
    std::vector<std::vector<double>> elements(rows, std::vector<double>(cols));
    return std::make_unique<Matrix>(rows, cols, elements, false);
}

std::unique_ptr<Matrix> Matrix::ParseSimple(const std::string& text)
{
    std::vector<std::vector<double>> elements;
    std::istringstream stream(text);
    std::string row;
    int cols = -1;
    while (getline(stream, row, '|')) {
        std::istringstream row_stream(row);
        std::vector<double> row_elements {
            std::istream_iterator<double>(row_stream),
            std::istream_iterator<double>()
        };
        if (cols == -1) {
            cols = row_elements.size();
            if (cols == 0) {
                throw IrgException("Cannot parse empty row!");
            }
        } else if (cols != (int) row_elements.size()) {
            throw IrgException("Conflicting column count values!");
        }
        cols = row_elements.size();
        elements.push_back(row_elements);
    }

    if (cols == -1) {
        std::vector<double> row_elements {
            std::istream_iterator<double>(stream),
            std::istream_iterator<double>()
        };
        elements.push_back(row_elements);
        cols = row_elements.size();
        if (cols == 0) {
            throw IrgException("Cannot parse empty matrix!");
        }
    }

    return std::make_unique<Matrix>(elements.size(), cols, elements, false);
}
