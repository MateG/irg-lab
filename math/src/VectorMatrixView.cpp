#include "VectorMatrixView.h"

#include "IMatrix.h"
#include "IrgException.h"

VectorMatrixView::VectorMatrixView(IMatrix& original)
: original_(original)
{
    int rows = original.GetRowsCount();
    int cols = original.GetColsCount();
    if (rows == 1) {
        row_matrix_ = true;
        dimension_ = cols;
    } else if (cols == 1) {
        row_matrix_ = false;
        dimension_ = rows;
    } else {
        throw IrgException("Given matrix cannot be viewed as a vector!");
    }
}

VectorMatrixView::~VectorMatrixView()
{
}

double VectorMatrixView::Get(int index) const
{
    return row_matrix_ ? original_.Get(0, index) : original_.Get(index, 0);
}

IVector& VectorMatrixView::Set(int index, double value)
{
    if (row_matrix_) {
        original_.Set(0, index, value);
    } else {
        original_.Set(index, 0, value);
    }
    return *this;
}

int VectorMatrixView::GetDimension() const
{
    return dimension_;
}

std::unique_ptr<IVector> VectorMatrixView::Copy() const
{
    return std::make_unique<VectorMatrixView>(original_);
}

std::unique_ptr<IVector> VectorMatrixView::NewInstance(int dimension) const
{
    throw IrgException("VectorMatrixView::NewInstance is not implemented!");
}
