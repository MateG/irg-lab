#include "IMatrix.h"

IMatrix::~IMatrix()
{
}

std::ostream& operator<<(std::ostream& os, const IMatrix& matrix)
{
    return matrix.print(os);
}
