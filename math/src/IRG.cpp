#include "IRG.h"

#include "Matrix.h"
#include "Vector.h"

std::unique_ptr<IMatrix> irg::Translate3D(float dx, float dy, float dz)
{
    return IRG_UNIQUE_MATRIX(
            {1.0, 0.0, 0.0, 0.0},
            {0.0, 1.0, 0.0, 0.0},
            {0.0, 0.0, 1.0, 0.0},
            {dx, dy, dz, 1.0}
    );
}

std::unique_ptr<IMatrix> irg::Scale3D(float sx, float sy, float sz)
{
    return IRG_UNIQUE_MATRIX(
            {sx, 0.0, 0.0, 0.0},
            {0.0, sy, 0.0, 0.0},
            {0.0, 0.0, sz, 0.0},
            {0.0, 0.0, 0.0, 1.0}
    );
}

std::unique_ptr<IMatrix> irg::LookAtMatrix(IVector& eye, IVector& center, IVector& view_up)
{
    std::unique_ptr<IVector> f = center.n_Sub(eye);
    f->Normalize();
    std::unique_ptr<IVector> up_normalized = view_up.n_Normalize();
    std::unique_ptr<IVector> s = f->n_VectorProduct(*up_normalized);
    s->Normalize();
    std::unique_ptr<IVector> u = s->n_VectorProduct(*f);
    u->Normalize();

    std::unique_ptr<IMatrix> M = IRG_UNIQUE_MATRIX(
            {s->Get(0), u->Get(0), -f->Get(0), 0.0},
            {s->Get(1), u->Get(1), -f->Get(1), 0.0},
            {s->Get(2), u->Get(2), -f->Get(2), 0.0},
            {0.0, 0.0, 0.0, 1.0}
    );
    std::unique_ptr<IMatrix> translate = irg::Translate3D(-eye.Get(0), -eye.Get(1), -eye.Get(2));
    return translate->n_Multiply(*M);
}

std::unique_ptr<IMatrix> irg::BuildFrustumMatrix(double l, double r, double b, double t, int n, int f)
{
    return IRG_UNIQUE_MATRIX(
            {2.0*n / (r-l), 0.0, 0.0, 0.0},
            {0.0, 2.0*n / (t-b), 0.0, 0.0},
            {(r+l) / (r-l), (t+b) / (t-b), (f+n) / (double) (n-f), -1.0},
            {0.0, 0.0, -2.0*f*n / (f-n), 0.0}
    );
}

bool irg::IsAntiClockwise(double a_x, double a_y, double b_x, double b_y, double c_x, double c_y)
{
    return (b_y - a_y) * (c_x - b_x) - (b_x - a_x) * (c_y - b_y) <= 0.0;
}
