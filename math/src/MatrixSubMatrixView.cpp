#include "MatrixSubMatrixView.h"

#include "IrgException.h"

MatrixSubMatrixView::MatrixSubMatrixView(IMatrix& original, int row, int col)
: original_(original)
{
    int rows = original.GetRowsCount();
    int cols = original.GetColsCount();
    if (row < 0 || row >= rows || col < 0 || col >= cols || rows <= 1 || cols <= 1) {
        throw IrgException("Sub matrix out of bounds!");
    }

    row_indexes_ = std::vector<int>(rows-1);
    int counter = 0;
    for (int i = 0; i < rows; i ++) {
        if (i != row) {
            row_indexes_[counter++] = i;
        }
    }

    col_indexes_ = std::vector<int>(cols-1);
    counter = 0;
    for (int i = 0; i < cols; i ++) {
        if (i != col) {
            col_indexes_[counter++] = i;
        }
    }
}

MatrixSubMatrixView::MatrixSubMatrixView(IMatrix& original,
        std::vector<int>& row_indexes, std::vector<int>& col_indexes)
: original_(original), row_indexes_(row_indexes), col_indexes_(col_indexes)
{
}

MatrixSubMatrixView::~MatrixSubMatrixView()
{
}

int MatrixSubMatrixView::GetRowsCount() const
{
    return row_indexes_.size();
}

int MatrixSubMatrixView::GetColsCount() const
{
    return col_indexes_.size();
}

double MatrixSubMatrixView::Get(int row, int col) const
{
    return original_.Get(row_indexes_[row], col_indexes_[col]);
}

IMatrix& MatrixSubMatrixView::Set(int row, int col, double value)
{
    original_.Set(row_indexes_[row], col_indexes_[col], value);
    return *this;
}

std::unique_ptr<IMatrix> MatrixSubMatrixView::Copy() const
{
    std::vector<int> row_indexes = row_indexes_;
    std::vector<int> col_indexes = col_indexes_;
    return std::unique_ptr<IMatrix>(new MatrixSubMatrixView(original_, row_indexes, col_indexes));
}

std::unique_ptr<IMatrix> MatrixSubMatrixView::NewInstance(int rows, int cols) const
{
    throw IrgException("MatrixSubMatrixView::NewInstance is not implemented!");
}

std::unique_ptr<IMatrix> MatrixSubMatrixView::SubMatrix(int row, int col, bool live_view)
{
    int rows = row_indexes_.size();
    int cols = col_indexes_.size();
    if (row < 0 || row >= rows || col < 0 || col >= cols || rows <= 1 || cols <= 1) {
        throw IrgException("Sub matrix out of bounds!");
    }

    if (live_view) {
        std::vector<int> row_indexes(rows-1);
        int counter = 0;
        for (int i = 0; i < rows; i ++) {
            if (i != row) {
                row_indexes[counter++] = row_indexes_[i];
            }
        }

        std::vector<int> col_indexes(cols-1);
        counter = 0;
        for (int i = 0; i < cols; i ++) {
            if (i != col) {
                col_indexes[counter++] = col_indexes_[i];
            }
        }
        return std::unique_ptr<MatrixSubMatrixView>(
                new MatrixSubMatrixView(original_, row_indexes, col_indexes));
    } else {
        std::unique_ptr<IMatrix> instance = original_.NewInstance(rows-1, cols-1);
        int counter_row = 0;
        for (int i = 0; i < rows; i ++) {
            if (i != row) {
                int counter_col = 0;
                for (int j = 0; j < cols; j ++) {
                    if (j != col) {
                        double value = original_.Get(row_indexes_[i], col_indexes_[j]);
                        instance->Set(counter_row, counter_col++, value);
                    }
                }
                counter_row ++;
            }
        }
        return instance;
    }
}
