#include "AbstractVector.h"

#include <iostream>
#include <cmath>
#include "Matrix.h"
#include "MatrixVectorView.h"
#include "IrgException.h"

AbstractVector::~AbstractVector()
{
}

std::unique_ptr<IVector> AbstractVector::CopyPart(int dimension) const
{
    std::unique_ptr<IVector> instance = NewInstance(dimension);
    for (int i = 0; i < dimension; i ++) {
        instance->Set(i, Get(i));
    }
    return instance;
}

IVector& AbstractVector::Add(const IVector& other)
{
    int dimension = GetDimension();
    if (dimension != other.GetDimension()) {
        throw IrgException("Vectors must be of same size!");
    }

    for (int i = 0; i < dimension; i ++) {
        Set(i, Get(i) + other.Get(i));
    }
    return *this;
}

std::unique_ptr<IVector> AbstractVector::n_Add(const IVector& other) const
{
    std::unique_ptr<IVector> copy = Copy();
    copy->Add(other);
    return copy;
}

IVector& AbstractVector::Sub(const IVector& other)
{
    int dimension = GetDimension();
    if (dimension != other.GetDimension()) {
        throw IrgException("Vectors must be of same size!");
    }

    for (int i = 0; i < dimension; i ++) {
        Set(i, Get(i) - other.Get(i));
    }
    return *this;
}

std::unique_ptr<IVector> AbstractVector::n_Sub(const IVector& other) const
{
    std::unique_ptr<IVector> copy = Copy();
    copy->Sub(other);
    return copy;
}

IVector& AbstractVector::ScalarMultiply(double scale)
{
    for (int i = 0; i < GetDimension(); i ++) {
        Set(i, Get(i)*scale);
    }
    return *this;
}

std::unique_ptr<IVector> AbstractVector::n_ScalarMultiply(double scale) const
{
    std::unique_ptr<IVector> copy = Copy();
    copy->ScalarMultiply(scale);
    return copy;
}

double AbstractVector::Norm() const
{
    double norm = 0.0;
    for (int i = 0; i < GetDimension(); i ++) {
        double component = (*this)[i];
        norm += component*component;
    }
    return sqrt(norm);
}

IVector& AbstractVector::Normalize()
{
    double normValue = Norm();
    if (normValue == 0.0) return *this; // Undefined?

    for (int i = 0; i < GetDimension(); i ++) {
        Set(i, Get(i)/normValue);
    }
    return *this;
}

std::unique_ptr<IVector> AbstractVector::n_Normalize() const
{
    std::unique_ptr<IVector> copy = Copy();
    copy->Normalize();
    return copy;
}

double AbstractVector::Cosine(const IVector& other) const
{
    double norm = Norm();
    if (norm == 0.0) return 0.0; // Undefined?
    double other_norm = other.Norm();
    if (other_norm == 0.0) return 0.0; // Undefined?
    return ScalarProduct(other) / (norm*other_norm);
}

double AbstractVector::ScalarProduct(const IVector& other) const
{
    int dimension = GetDimension();
    if (dimension != other.GetDimension()) {
        throw;
    }

    double product = 0.0;
    for (int i = 0; i < dimension; i ++) {
        product += (*this)[i] * other[i];
    }
    return product;
}

std::unique_ptr<IVector> AbstractVector::n_VectorProduct(const IVector& other) const
{
    if (GetDimension() != 3 || other.GetDimension() != 3) {
        throw IrgException("Vector product is defined only for 3-dimensional vectors!");
    }

    std::unique_ptr<IVector> product = NewInstance(3);
    product->Set(0, (*this)[1]*other[2] - (*this)[2]*other[1]);
    product->Set(1, (*this)[2]*other[0] - (*this)[0]*other[2]);
    product->Set(2, (*this)[0]*other[1] - (*this)[1]*other[0]);
    return product;
}

std::unique_ptr<IVector> AbstractVector::n_FromHomogenous() const
{
    int dimension = GetDimension();
    std::unique_ptr<IVector> copy = CopyPart(dimension-1);
    copy->ScalarMultiply(1.0/(*this)[dimension-1]);
    return copy;
}

std::unique_ptr<IMatrix> AbstractVector::ToRowMatrix(bool live_view)
{
    if (live_view) {
        return std::make_unique<MatrixVectorView>(*this, true);
    }

    int dimension = GetDimension();
    std::vector<double> row(dimension);
    for (int i = 0; i < dimension; i ++) {
        row[i] = Get(i);
    }
    std::vector<std::vector<double>> elements = {row};
    return std::make_unique<Matrix>(1, dimension, elements, false);
}

std::unique_ptr<IMatrix> AbstractVector::ToColumnMatrix(bool live_view)
{
    if (live_view) {
        return std::make_unique<MatrixVectorView>(*this, false);
    }

    int dimension = GetDimension();
    std::vector<std::vector<double>> elements(dimension, std::vector<double>(1));
    for (int i = 0; i < dimension; i ++) {
        elements[i][0] = Get(i);
    }
    return std::make_unique<Matrix>(dimension, 1, elements, false);
}

std::vector<double> AbstractVector::ToArray() const
{
    int dimension = GetDimension();
    std::vector<double> array(dimension);
    for (int i = 0; i < dimension; i ++) {
        array[i] = (*this)[i];
    }
    return array;
}

std::ostream& AbstractVector::print(std::ostream& os) const
{
    int dimension = GetDimension();
    os << std::fixed;
    os << '[' << (*this)[0];
    for (int i = 1; i < dimension; i ++) {
        os << ", " << (*this)[i];
    }
    os << ']';
    return os;
}
