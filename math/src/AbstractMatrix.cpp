#include "AbstractMatrix.h"

#include <iostream>
#include "MatrixTransposeView.h"
#include "MatrixSubMatrixView.h"
#include "VectorMatrixView.h"
#include "Vector.h"
#include "IrgException.h"

AbstractMatrix::~AbstractMatrix()
{
}

std::unique_ptr<IMatrix> AbstractMatrix::n_Transpose(bool live_view)
{
    if (live_view) {
        return std::make_unique<MatrixTransposeView>(*this);
    }

    int rows = GetRowsCount();
    int cols = GetColsCount();
    std::unique_ptr<IMatrix> instance = NewInstance(cols, rows);
    for (int row = 0; row < rows; row ++) {
        for (int col = 0; col < cols; col ++) {
            instance->Set(col, row, Get(row, col));
        }
    }
    return instance;
}

IMatrix& AbstractMatrix::Add(const IMatrix& other)
{
    int rows = GetRowsCount();
    int cols = GetColsCount();
    if (rows != other.GetRowsCount() || cols != other.GetColsCount()) {
        throw IrgException("Matrices must be of same size!");
    }

    for (int col = 0; col < cols; col ++) {
        for (int row = 0; row < rows; row ++) {
            Set(row, col, Get(row, col) + other.Get(row, col));
        }
    }
    return *this;
}

std::unique_ptr<IMatrix> AbstractMatrix::n_Add(const IMatrix& other) const
{
    std::unique_ptr<IMatrix> copy = Copy();
    copy->Add(other);
    return copy;
}

IMatrix& AbstractMatrix::Sub(const IMatrix& other)
{
    int rows = GetRowsCount();
    int cols = GetColsCount();
    if (rows != other.GetRowsCount() || cols != other.GetColsCount()) {
        throw IrgException("Matrices must be of same size!");
    }

    for (int row = 0; row < rows; row ++) {
        for (int col = 0; col < cols; col ++) {
            Set(row, col, Get(row, col) - other.Get(row, col));
        }
    }
    return *this;
}

std::unique_ptr<IMatrix> AbstractMatrix::n_Sub(const IMatrix& other) const
{
    std::unique_ptr<IMatrix> copy = Copy();
    copy->Sub(other);
    return copy;
}

std::unique_ptr<IMatrix> AbstractMatrix::n_Multiply(const IMatrix& other) const
{
    int rows_this = GetRowsCount();
    int cols_this = GetColsCount();
    int rows_other = other.GetRowsCount();
    int cols_other = other.GetColsCount();
    if (cols_this != rows_other) {
        throw IrgException("Matrices incompatible for multiplication!");
    }

    std::unique_ptr<IMatrix> instance = NewInstance(rows_this, cols_other);
    for (int row = 0; row < rows_this; row ++) {
        for (int col = 0; col < cols_other; col ++) {
            double value = 0.0;
            for (int i = 0; i < cols_this; i ++) {
                value += Get(row, i) * other.Get(i, col);
            }
            instance->Set(row, col, value);
        }
    }
    return instance;
}

double AbstractMatrix::Determinant()
{
    int rows = GetRowsCount();
    if (rows != GetColsCount()) {
        throw IrgException("Determinant is defined only for square matrices!");
    } else if (rows == 1) {
        return Get(0, 0);
    } else if (rows == 2) {
        return Get(0, 0) * Get(1, 1) - Get(0, 1) * Get(1, 0);
    } else if (rows == 3) {
        return Get(0, 0) * (Get(1, 1)*Get(2, 2) - Get(1, 2)*Get(2, 1))
             - Get(0, 1) * (Get(1, 0)*Get(2, 2) - Get(1, 2)*Get(2, 0))
             + Get(0, 2) * (Get(1, 0)*Get(2, 1) - Get(1, 1)*Get(2, 0));
    }

    double determinant = 0.0;
    for (int col = 0; col < rows; col ++) {
        std::unique_ptr<IMatrix> sub = SubMatrix(0, col, true);
        double component = Get(0, col) * sub->Determinant();
        if (col % 2) component = -component;
        determinant += component;
    }
    return determinant;
}

std::unique_ptr<IMatrix> AbstractMatrix::SubMatrix(int row, int col, bool live_view)
{
    if (live_view) {
        return std::make_unique<MatrixSubMatrixView>(*this, row, col);
    }

    int rows = GetRowsCount();
    int cols = GetColsCount();
    if (row < 0 || row >= rows || col < 0 || col >= cols || rows <= 1 || cols <= 1) {
        throw IrgException("Sub matrix out of bounds!");
    }

    std::unique_ptr<IMatrix> instance = NewInstance(rows-1, cols-1);
    int counter_row = 0;
    for (int i = 0; i < rows; i ++) {
        if (i != row) {
            int counter_col = 0;
            for (int j = 0; j < cols; j ++) {
                if (j != col) {
                    instance->Set(counter_row, counter_col++, Get(i, j));
                }
            }
            counter_row ++;
        }
    }
    return instance;
}

std::unique_ptr<IMatrix> AbstractMatrix::n_Invert()
{
    int rows = GetRowsCount();
    if (rows != GetColsCount()) {
        throw IrgException("Inverse is defined only for square matrices!");
    }

    std::unique_ptr<IMatrix> inverse = NewInstance(rows, rows);
    for (int row = 0; row < rows; row ++) {
        for (int col = 0; col < rows; col ++) {
            std::unique_ptr<IMatrix> sub = SubMatrix(row, col, true);
            double cofactor = sub->Determinant();
            if (row % 2 != col % 2) cofactor = -cofactor;
            inverse->Set(col, row, cofactor); // Transpose on the fly.
        }
    }

    double determinant = 0.0;
    for (int i = 0; i < rows; i ++) {
        determinant += Get(0, i)*inverse->Get(i, 0);
    }

    if (determinant == 0.0) {
        throw IrgException("Inverse is undefined for singular matrices!");
    }

    for (int row = 0; row < rows; row ++) {
        for (int col = 0; col < rows; col ++) {
            inverse->Set(row, col, inverse->Get(row, col) / determinant);
        }
    }
    return inverse;
}

std::vector<std::vector<double>> AbstractMatrix::ToArray() const
{
    int rows = GetRowsCount();
    int cols = GetColsCount();
    std::vector<std::vector<double>> array(rows, std::vector<double>(cols));
    for (int row = 0; row < rows; row ++) {
        for (int col = 0; col < cols; col ++) {
            array[row][col] = Get(row, col);
        }
    }
    return array;
}

std::unique_ptr<IVector> AbstractMatrix::ToVector(bool live_view)
{
    int rows = GetRowsCount();
    int cols = GetColsCount();
    if (rows != 1 && cols != 1) {
        throw IrgException("This matrix cannot be viewed as a vector!");
    }

    if (live_view) {
        return std::make_unique<VectorMatrixView>(*this);
    }

    std::vector<double> elements;
    if (rows == 1) {
        elements = std::vector<double>(cols);
        for (int col = 0; col < cols; col ++) {
            elements[col] = Get(0, col);
        }
    } else {
        elements = std::vector<double>(rows);
        for (int row = 0; row < rows; row ++) {
            elements[row] = Get(row, 0);
        }
    }
    return std::make_unique<Vector>(false, false, elements);
}

std::ostream& AbstractMatrix::print(std::ostream& os) const
{
    int rows = GetRowsCount();
    int cols = GetColsCount();
    os << std::fixed;
    for (int row = 0; row < rows; row ++) {
        os << '[' << Get(row, 0);
        for (int col = 1; col < cols; col ++) {
            os << ", " << Get(row, col);
        }
        os << ']';
        if (row != rows-1) os << '\n';
    }
    return os;
}
