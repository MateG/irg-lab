#ifndef IVECTOR_H
#define IVECTOR_H

#include <memory>
#include <vector>
class IMatrix;

class IVector
{
private:
    virtual std::ostream& print(std::ostream& os) const = 0;
public:
    virtual ~IVector();

    virtual double operator[](int index) const;

    virtual double Get(int index) const = 0;
    virtual IVector& Set(int index, double value) = 0;
    virtual int GetDimension() const = 0;
    virtual std::unique_ptr<IVector> Copy() const = 0;
    virtual std::unique_ptr<IVector> CopyPart(int dimension) const = 0;
    virtual std::unique_ptr<IVector> NewInstance(int dimension) const = 0;
    virtual IVector& Add(const IVector& other) = 0;
    virtual std::unique_ptr<IVector> n_Add(const IVector& other) const = 0;
    virtual IVector& Sub(const IVector& other) = 0;
    virtual std::unique_ptr<IVector> n_Sub(const IVector& other) const = 0;
    virtual IVector& ScalarMultiply(double scale) = 0;
    virtual std::unique_ptr<IVector> n_ScalarMultiply(double scale) const = 0;
    virtual double Norm() const = 0;
    virtual IVector& Normalize() = 0;
    virtual std::unique_ptr<IVector> n_Normalize() const = 0;
    virtual double Cosine(const IVector& other) const = 0;
    virtual double ScalarProduct(const IVector& other) const = 0;
    virtual std::unique_ptr<IVector> n_VectorProduct(const IVector& other) const = 0;
    virtual std::unique_ptr<IVector> n_FromHomogenous() const = 0;
    virtual std::unique_ptr<IMatrix> ToRowMatrix(bool live_view) = 0;
    virtual std::unique_ptr<IMatrix> ToColumnMatrix(bool live_view) = 0;
    virtual std::vector<double> ToArray() const = 0;

    friend std::ostream& operator<<(std::ostream& os, const IVector& vector);
};

#endif // IVECTOR_H
