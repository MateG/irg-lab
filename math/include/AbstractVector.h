#ifndef ABSTRACTVECTOR_H
#define ABSTRACTVECTOR_H

#include "IVector.h"

class AbstractVector : public IVector
{
private:
    std::ostream& print(std::ostream& os) const override;
public:
    virtual ~AbstractVector();

    std::unique_ptr<IVector> CopyPart(int dimension) const override;
    IVector& Add(const IVector& other) override;
    std::unique_ptr<IVector> n_Add(const IVector& other) const override;
    IVector& Sub(const IVector& other) override;
    std::unique_ptr<IVector> n_Sub(const IVector& other) const override;
    IVector& ScalarMultiply(double scale) override;
    std::unique_ptr<IVector> n_ScalarMultiply(double scale) const override;
    double Norm() const override;
    IVector& Normalize() override;
    std::unique_ptr<IVector> n_Normalize() const override;
    double Cosine(const IVector& other) const override;
    double ScalarProduct(const IVector& other) const override;
    std::unique_ptr<IVector> n_VectorProduct(const IVector& other) const override;
    std::unique_ptr<IVector> n_FromHomogenous() const override;
    std::unique_ptr<IMatrix> ToRowMatrix(bool live_view) override;
    std::unique_ptr<IMatrix> ToColumnMatrix(bool live_view) override;
    std::vector<double> ToArray() const override;
};

#endif // ABSTRACTVECTOR_H
