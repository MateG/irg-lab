#ifndef IRG_H
#define IRG_H

#include <memory>
class IMatrix;
class IVector;

namespace irg {
    std::unique_ptr<IMatrix> Translate3D(float dx, float dy, float dz);
    std::unique_ptr<IMatrix> Scale3D(float sx, float sy, float sz);
    std::unique_ptr<IMatrix> LookAtMatrix(IVector& eye, IVector& center, IVector& view_up);
    std::unique_ptr<IMatrix> BuildFrustumMatrix(double l, double r, double b, double t, int n, int f);
    bool IsAntiClockwise(double a_x, double a_y, double b_x, double b_y, double c_x, double c_y);
}

#endif // IRG_H
