#ifndef VECTORMATRIXVIEW_H
#define VECTORMATRIXVIEW_H

#include "AbstractVector.h"

class VectorMatrixView : public AbstractVector
{
private:
    IMatrix& original_;
    int dimension_;
    bool row_matrix_;
public:
    VectorMatrixView(IMatrix& original);
    virtual ~VectorMatrixView();

    double Get(int index) const override;
    IVector& Set(int index, double value) override;
    int GetDimension() const override;
    std::unique_ptr<IVector> Copy() const override;
    std::unique_ptr<IVector> NewInstance(int dimension) const override;
};

#endif // VECTORMATRIXVIEW_H
