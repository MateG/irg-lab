#ifndef MATRIXTRANSPOSEVIEW_H
#define MATRIXTRANSPOSEVIEW_H

#include "AbstractMatrix.h"

class MatrixTransposeView : public AbstractMatrix
{
private:
    IMatrix& original_;
public:
    MatrixTransposeView(IMatrix& original);
    virtual ~MatrixTransposeView();

    int GetRowsCount() const override;
    int GetColsCount() const override;
    double Get(int row, int col) const override;
    IMatrix& Set(int row, int col, double value) override;
    std::unique_ptr<IMatrix> Copy() const override;
    std::unique_ptr<IMatrix> NewInstance(int rows, int cols) const override;
};

#endif // MATRIXTRANSPOSEVIEW_H
