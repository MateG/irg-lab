#include <vector>
#include <GL/glut.h>

void reshape(int width, int height);
void display();
void renderScene();

void keyPressed(unsigned char key, int x, int y);
void mousePressedOrReleased(int button, int state, int x, int y);
void mouseMoved(int x, int y);

const int color_indicator_size = 5;
struct Color {
    float r, g, b;
};
const int color_count = 6;
Color colors[color_count] = {
    {1.0f, 0.0f, 0.0f}, // Red
    {0.0f, 1.0f, 0.0f}, // Green
    {0.0f, 0.0f, 1.0f}, // Blue
    {0.0f, 1.0f, 1.0f}, // Cyan
    {1.0f, 1.0f, 0.0f}, // Yellow
    {1.0f, 0.0f, 1.0f}  // Magenta
};
int color_index = 0;
Color current_color = colors[color_index];

struct Point {
    int x, y;
};
struct Triangle {
    Point a, b, c;
    Color color;
};
std::vector<Triangle> triangles;

int click_counter = 0;
Point a, b, mouse;

int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE);
    glutInitWindowSize(800, 600);
    glutInitWindowPosition(400, 100);
    glutCreateWindow("Example 1");

    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutKeyboardFunc(keyPressed);
    glutMouseFunc(mousePressedOrReleased);
    glutPassiveMotionFunc(mouseMoved);

    glutMainLoop();
    return 0;
}

void reshape(int width, int height)
{
    glDisable(GL_DEPTH_TEST);
    glViewport(0, 0, (GLsizei) width, (GLsizei) height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, width-1, height-1, 0, 0, 1);
    glMatrixMode(GL_MODELVIEW);
}

void display()
{
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    glLoadIdentity();
    renderScene();
    glutSwapBuffers();
}

void renderScene()
{
    glPointSize(1.0f);
    for (Triangle triangle : triangles) {
        glColor3f(triangle.color.r, triangle.color.g, triangle.color.b);
        glBegin(GL_TRIANGLES);
        glVertex2i(triangle.a.x, triangle.a.y);
        glVertex2i(triangle.b.x, triangle.b.y);
        glVertex2i(triangle.c.x, triangle.c.y);
        glEnd();
    }

    if (click_counter == 1) {
        glColor3f(current_color.r, current_color.g, current_color.b);
        glBegin(GL_LINES);
        glVertex2i(a.x, a.y);
        glVertex2i(mouse.x, mouse.y);
        glEnd();
    } else if (click_counter == 2) {
        glColor3f(current_color.r, current_color.g, current_color.b);
        glBegin(GL_TRIANGLES);
        glVertex2i(a.x, a.y);
        glVertex2i(b.x, b.y);
        glVertex2i(mouse.x, mouse.y);
        glEnd();
    }

    int last = glutGet(GLUT_WINDOW_WIDTH) - 1;
    glColor3f(current_color.r, current_color.g, current_color.b);
    glBegin(GL_QUADS);
    glVertex2i(last - color_indicator_size, 0);
    glVertex2i(last, 0);
    glVertex2i(last, color_indicator_size);
    glVertex2i(last - color_indicator_size, color_indicator_size);
    glEnd();
}

void keyPressed(unsigned char key, int x, int y)
{
    if (key == 'n') {
        color_index ++;
        color_index %= color_count;
        current_color = colors[color_index];
        glutPostRedisplay();
    } else if (key == 'p') {
        color_index --;
        color_index = (color_index + color_count) % color_count;
        current_color = colors[color_index];
        glutPostRedisplay();
    }
}

void mousePressedOrReleased(int button, int state, int x, int y)
{
    if (button == GLUT_LEFT_BUTTON) {
        if (state == GLUT_UP) {
            if (click_counter == 0) {
                a.x = x;
                a.y = y;
            } else if (click_counter == 1) {
                b.x = x;
                b.y = y;
            } else {
                triangles.push_back({a, b, {x, y}, current_color});
            }
            click_counter = (click_counter + 1) % 3;
            glutPostRedisplay();
        }
    }
}

void mouseMoved(int x, int y)
{
    mouse.x = x;
    mouse.y = y;
    if (click_counter > 0) glutPostRedisplay();
}
