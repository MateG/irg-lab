#include "common.h"
#include "IRG/Matrix.h"
#include "IRG/Vector.h"
#include "IRG/IRG.h"

std::unique_ptr<IVector> transposeVertex(Vertex3D& vertex);
bool isAntiClockwise(IVector& tv_a, IVector& tv_b, IVector& tv_c);

Vector eye = {3.0, 4.0, 1.0};
Vector center = {0.0, 0.0, 0.0};
Vector view_up = {0.0, 1.0, 0.0};
std::unique_ptr<IMatrix> look_at = irg::LookAtMatrix(eye, center, view_up);
std::unique_ptr<IMatrix> frustum = irg::BuildFrustumMatrix(-0.5, 0.5, -0.5, 0.5, 1, 100);
std::unique_ptr<IMatrix> m = look_at->n_Multiply(*frustum);

int main(int argc, char** argv)
{
    return launch(argc, argv, "Task 2");
}

void reshape(int width, int height)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glViewport(0, 0, width, height);
}

void display()
{
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    if (culling == algorithm_1) {
        model->DetermineFaceVisibilities1(eye);
    } else if (culling == algorithm_2) {
        model->DetermineFaceVisibilities2(eye);
    }

    renderScene();
    glutSwapBuffers();
}

void renderScene()
{
    glColor3f(1.0f, 0.0f, 0.0f);
    std::vector<Vertex3D>& vertices = model->GetVertices();
    for (const Face3D& triangle : model->GetTriangles()) {
        if (culling == algorithm_1 || culling == algorithm_2) {
            if (!triangle.visible) continue;
        }

        auto tv_a = transposeVertex(vertices[triangle.indexes[0]]);
        auto tv_b = transposeVertex(vertices[triangle.indexes[1]]);
        auto tv_c = transposeVertex(vertices[triangle.indexes[2]]);
        if (culling == algorithm_3 && !isAntiClockwise(*tv_a, *tv_b, *tv_c)) continue;

        glBegin(GL_LINE_LOOP);
        glVertex2f((float) tv_a->Get(0), (float) tv_a->Get(1));
        glVertex2f((float) tv_b->Get(0), (float) tv_b->Get(1));
        glVertex2f((float) tv_c->Get(0), (float) tv_c->Get(1));
        glEnd();
    }
}

void updateEye(double x, double z)
{
    eye.Set(0, x);
    eye.Set(2, z);
    look_at = irg::LookAtMatrix(eye, center, view_up);
    m = look_at->n_Multiply(*frustum);
}

std::unique_ptr<IVector> transposeVertex(Vertex3D& vertex)
{
    Vector v = {vertex.x, vertex.y, vertex.z, 1.0};
    return v.ToRowMatrix(false)->n_Multiply(*m)->ToVector(false)->n_FromHomogenous();
}

bool isAntiClockwise(IVector& tv_a, IVector& tv_b, IVector& tv_c)
{
    return irg::IsAntiClockwise(tv_a.Get(0), tv_a.Get(1), tv_b.Get(0), tv_b.Get(1), tv_c.Get(0), tv_c.Get(1));
}
