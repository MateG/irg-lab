#ifndef MATRIXVECTORVIEW_H
#define MATRIXVECTORVIEW_H

#include "AbstractMatrix.h"

class MatrixVectorView : public AbstractMatrix
{
private:
    IVector& original_;
    bool as_row_matrix_;
public:
    MatrixVectorView(IVector& original, bool as_row_matrix);
    virtual ~MatrixVectorView();

    int GetRowsCount() const override;
    int GetColsCount() const override;
    double Get(int row, int col) const override;
    IMatrix& Set(int row, int col, double value) override;
    std::unique_ptr<IMatrix> Copy() const override;
    std::unique_ptr<IMatrix> NewInstance(int rows, int cols) const override;
};

#endif // MATRIXVECTORVIEW_H
