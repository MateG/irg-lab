#ifndef MATRIX_H
#define MATRIX_H

#include "AbstractMatrix.h"

#define IRG_UNIQUE_MATRIX(x, ...) \
    (std::make_unique<Matrix>(std::initializer_list<std::initializer_list<double>>({x, __VA_ARGS__})))

class Matrix : public AbstractMatrix
{
private:
    std::unique_ptr<std::vector<std::vector<double>>> elements_;
    int rows_;
    int cols_;
public:
    Matrix(int rows, int cols);
    Matrix(int rows, int cols, std::vector<std::vector<double>>& elements, bool store_reference);
    Matrix(const std::initializer_list<std::initializer_list<double>>& list);
    virtual ~Matrix();

    int GetRowsCount() const override;
    int GetColsCount() const override;
    double Get(int row, int col) const override;
    IMatrix& Set(int row, int col, double value) override;
    std::unique_ptr<IMatrix> Copy() const override;
    std::unique_ptr<IMatrix> NewInstance(int rows, int cols) const override;

    static std::unique_ptr<Matrix> ParseSimple(const std::string& text);
};

#endif // MATRIX_H
