#ifndef COMMON_H
#define COMMON_H

#include <iostream>
#include <cmath>
#include <memory>
#include <GL/glut.h>
#include "ObjectModel.h"

void reshape(int width, int height);
void display();
void renderScene();
void keyPressed(unsigned char key, int x, int y);

void updateEye(double x, double z);

std::unique_ptr<ObjectModel> model;

const double ANGLE_START = 18.4349488;
double angle = ANGLE_START, increment = 1.0, r = 3.16227766;

enum Culling {
    none, algorithm_1, algorithm_2, algorithm_3
};
Culling culling = none;

int launch(int argc, char** argv, const char* title)
{
    if (argc != 2) {
        std::cerr << "Expected a single argument - path to the .obj file." << std::endl;
        return 1;
    }
    model = ObjectModel::ParseFromFile(argv[1]);
    model->Normalize();

    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE);
    glutInitWindowSize(640, 480);
    glutInitWindowPosition(200, 100);
    glutCreateWindow(title);

    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutKeyboardFunc(keyPressed);

    glutMainLoop();
    return 0;
}

#define TO_RADIANS(x) (x*0.0174532925)
void keyPressed(unsigned char key, int x, int y)
{
    if (key == 'r') {
        angle += increment;
        double radians = TO_RADIANS(angle);
        updateEye(r*std::cos(radians), r*std::sin(radians));
        glutPostRedisplay();
    } else if (key == 'l') {
        angle -= increment;
        double radians = TO_RADIANS(angle);
        updateEye(r*std::cos(radians), r*std::sin(radians));
        glutPostRedisplay();
    } else if (key == 27) {
        angle = 18.4349488;
        double radians = TO_RADIANS(angle);
        updateEye(r*std::cos(radians), r*std::sin(radians));
        glutPostRedisplay();
    } else if (key == '1') {
        culling = none;
        glutPostRedisplay();
    } else if (key == '2') {
        culling = algorithm_1;
        glutPostRedisplay();
    } else if (key == '3') {
        culling = algorithm_2;
        glutPostRedisplay();
    } else if (key == '4') {
        culling = algorithm_3;
        glutPostRedisplay();
    }
}
#undef TO_RADIANS

#endif // COMMON_H
